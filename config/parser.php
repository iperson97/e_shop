<?php

return [
    'al_style_key' => env('AL_STYLE_KZ_KEY', null),
    'sleep_time' => env('PARSER_SLEEP_TIME', 180),
    'sleep_count' => env('PARSER_SLEEP_COUNT', 100),
];
