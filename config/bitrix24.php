<?php

return [
    'client_id' => env('BITRIX24_CLIENT_ID', 'local.5fabdfd28ff422.70262111'),
    'client_secret' => env('BITRIX24_CLIENT_SECRET', 'eY3AaNSFLZl9Io5oQgoGFAZrkAn251KVyTTrDPcZfgShQt8unS'),
    'current_encoding' => env('BITRIX24_CURRENT_ENCODING', null),
    'web_hook_url' => env('BITRIX24_WEB_HOOK_URL', null),
    'ignore_ssl' => env('BITRIX24_IGNORE_SSL', true),
    'log_type_dump' => env('BITRIX24_LOG_TYPE_DUMP', false),
    'block_log' => env('BITRIX24_BLOCK_LOG',true),
    'logs_dir' => env('BITRIX24_LOGS_DIR', storage_path('app/bitrix')),

];
