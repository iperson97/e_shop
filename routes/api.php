<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get("updateCategories",[\App\Http\Controllers\updateDbController::class,"updateCategories"]);
Route::get("updateGoods",[\App\Http\Controllers\updateDbController::class,"updateGoods"]);
Route::get("updateDescription",[\App\Http\Controllers\updateDbController::class,"updateDescription"]);
Route::get('catalog/{category:slug}/products2', [ProductController::class, 'index2'])->name('products');
