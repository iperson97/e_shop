<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserFavoriteController;
use App\Http\Controllers\ProductComparisonController;
use App\Http\Controllers\BitrixController;
use App\Http\Controllers\FilterController;
use App\Http\Controllers\ListController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'welcome'])->name('welcome');
Route::get('/category/list', [HomeController::class, 'categoryList'])->name('category.list');

Route::get('/service', [HomeController::class, 'service'])->name('service');
Route::get('/about-us', [HomeController::class, 'aboutUs'])->name('about-us');
Route::get('/about-delivery', [HomeController::class, 'aboutDelivery'])->name('about-delivery');
Route::get('/security-policy', [HomeController::class, 'securityPolicy'])->name('security-policy');
Route::get('/terms-of-agreement', [HomeController::class, 'termsAgreement'])->name('terms');
Route::get('/contract-offer', [HomeController::class, 'contractOffer'])->name('contract-offer');

Route::post('/zayavka', [HomeController::class, 'zayavka'])->name('zayavka');

Route::prefix('register')->name('register.')->group(function() {
    Route::post('/code', [AuthController::class, 'registerCode'])->name('code');
    Route::post('/confirm-code', [AuthController::class, 'confirmCode'])->name('confirm.code');
});


Route::get('/search', [ProductController::class, 'search'])->name('search');
Route::get('/search/products', [ProductController::class, 'searchProducts'])->name('search.products');
Route::prefix('catalog')->name('catalog.')->group(function() {
    Route::get('/', [ProductController::class, 'catalog'])->name('index');
    Route::get('/{category:slug}', [ProductController::class, 'view'])->name('view');
    Route::get('/{category:slug}/products', [ProductController::class, 'index'])->name('products');
});


Route::prefix('filter')->name('filter.')->group(function() {
    Route::get('/{category}/brands', [FilterController::class, 'categoryBrands'])->name('brands');
    Route::get('/{category}/properties', [FilterController::class, 'categoryProperties'])->name('properties');
    Route::get('/{category}/options', [FilterController::class, 'categoryFilters'])->name('options');
    Route::get('/{city}/delivery-methods', [FilterController::class, 'cityDeliveryMethods'])->name('city-delivery-methods');
    Route::get('/{delivery}/payment-methods', [FilterController::class, 'deliveryPaymentMethods'])->name('delivery-payment-methods');
});

Route::prefix('products')->name('products.')->group(function() {
    Route::get('/recommendations', [ProductController::class, 'recommendations'])->name('recommendations');
    Route::get('/new-items', [ProductController::class, 'newItems'])->name('new-items');
    Route::get('/{product:slug}', [ProductController::class, 'show'])->name('show');
    Route::middleware(['auth:sanctum', 'verified'])->group(function() {
        Route::get('/{product:slug}/review', [ProductController::class, 'review'])->name('review');
        Route::post('/{product}/review', [ProductController::class, 'storeReview'])->name('store-review');
    });
});

Route::post('/fast/order', [ProductController::class, 'fastOrder'])->name('fast-order');

Route::prefix('cart')->name('cart.')->group(function() {
    Route::get('/', [CartController::class, 'index'])->name('index');
    Route::get('/count', [CartController::class, 'count'])->name('count');
    Route::post('/', [CartController::class, 'add'])->name('add');
    Route::put('/{product}', [CartController::class, 'update'])->name('update');
    Route::post('/conditions', [CartController::class, 'addCondition'])->name('addCondition');
    Route::delete('/conditions', [CartController::class, 'clearCartConditions'])->name('clearCartConditions');
    Route::get('/details', [CartController::class, 'details'])->name('details');
    Route::delete('/{id}', [CartController::class, 'delete'])->name('delete');

    Route::get('/checkout', [CartController::class, 'checkoutView'])->name('checkout.view');
    Route::post('/checkout', [CartController::class, 'checkout'])->name('checkout');
});


Route::prefix('favorites')->name('favorites.')->group(function() {
    Route::get('/', [UserFavoriteController::class, 'index'])->name('index');
    Route::get('/count', [UserFavoriteController::class, 'count'])->name('count');
    Route::post('/add-or-remove/{product}', [UserFavoriteController::class, 'addOrRemove'])->name('add-or-remove');
    Route::delete('/remove/{product}', [UserFavoriteController::class, 'remove'])->name('remove');
});

Route::prefix('comparison')->name('comparison.')->group(function() {
    Route::get('/', [ProductComparisonController::class, 'index'])->name('index');
    Route::get('/count', [ProductComparisonController::class, 'count'])->name('count');
    Route::post('/add/{product}', [ProductComparisonController::class, 'add'])->name('add');
    Route::delete('/remove/{product}', [ProductComparisonController::class, 'remove'])->name('remove');
});

Route::prefix('personal')->name('personal.')->middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('index');
    Route::get('/profile', [UserController::class, 'profile'])->name('profile');
    Route::get('/orders', [UserController::class, 'orders'])->name('orders');
    Route::get('/update-password', [UserController::class, 'updatePassword'])->name('update-password');
});

Route::prefix('list')->name('list.')->middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/properties', [ListController::class, 'properties'])->name('properties');
    Route::get('/properties/{property}/values', [ListController::class, 'selectedPropertyValues'])->name('selected.property.values');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::prefix('bitrix24')->name('bitrix24.')->group(function() {
    Route::get('/handler', [BitrixController::class, 'index'])->name('handler');
});

// Route::get('test', [ProductController::class, 'test']);
