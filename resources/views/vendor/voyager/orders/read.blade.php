@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->getTranslatedAttribute('display_name_singular')) }} &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('browse', $dataTypeContent)
        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
        @endcan
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered {{ $order->name && $order->phone ? 'panel-primary' : 'panel-info'}}" style="padding-bottom:5px;">
                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">Заказ  #{{ $order->id }} {{ $order->name && $order->phone ? '(Быстрый заказ)' : '' }}</h3>
                    </div>
                    <div class="panel-body row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h4>ФИО</h4>
                                <p>{{ $order->user ? $order->user->fullname : $order->name }}</p>
                            </div>
                            <div class="form-group">
                                <h4>Номер телефона</h4>
                                <p>{{ $order->user ? $order->user->phone : $order->phone }}</p>
                            </div>
                            <div class="form-group">
                                <h4>Дата заказа</h4>
                                <p>{{ $order->created_at->format('d.m.Y H:i') }}</p>
                            </div>

                            <div class="form-group">
                                <h4>Способы оплаты</h4>
                                <p>{{ $order->paymentMethod ? $order->paymentMethod->name : 'Быстрый заказ' }}</p>
                            </div>
                            <div class="form-group">
                                <h4>Способы доставки</h4>
                                <p>{{ $order->deliveryMethod ? $order->deliveryMethod->name : 'Быстрый заказ' }}</p>
                            </div>

                            @if ($order->address)
                                <div class="form-group">
                                    <h4>Aдрес</h4>

                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td> <strong>Населенный пункт:</strong> </td>
                                                    <td>{{ $order->address->city->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td> <strong>Улица:</strong> </td>
                                                    <td>{{ $order->address->street }}</td>
                                                </tr>
                                                <tr>
                                                    <td> <strong>Дом:</strong> </td>
                                                    <td>{{ $order->address->house }}</td>
                                                </tr>
                                                <tr>
                                                    <td> <strong>Подъезд:</strong> </td>
                                                    <td>{{ $order->address->entrance }}</td>
                                                </tr>
                                                <tr>
                                                    <td> <strong>Этаж:</strong> </td>
                                                    <td>{{ $order->address->floor }}</td>
                                                </tr>
                                                <tr>
                                                    <td> <strong>Квартира:</strong> </td>
                                                    <td>{{ $order->address->apartment }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                            @endif
                            @if ($order->notes)
                                <div class="form-group">
                                    <h4>Комментарий</h4>
                                    <p>{{ $order->notes }}</p>
                                </div>
                            @endif

                            <div class="form-group row">
                                <div class="col-md-6 col-6">
                                    <h4>Оплачено</h4>
                                    <p>{{ $order->paid ? 'Да': 'Нет' }}</p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <h4>Доставлено</h4>
                                    <p>{{ $order->delivered ? 'Да': 'Нет' }}</p>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-8">
                            <div class="table-responsive">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Продукт</th>
                                                <th>Итого</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($order->products as $key => $product)
                                                <tr>
                                                    <td> {{ $product->product ? $product->product->name : ''}} <strong> × {{ $product->quantity}}</strong></td>
                                                    <td> {{ $product->total }} ₸</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr class="order_total">
                                                <th>Предварительная стоимость</th>
                                                <td><strong>{{ $order->sub_total }} ₸</strong></td>
                                            </tr>
                                            <tr class="order_total">
                                                <th>Итого</th>
                                                <td><strong>{{ $order->total }} ₸</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

    </script>
@stop
