<br>
@if(isset($dataTypeContent->{$row->field}))
    @if($dataTypeContent->{$row->field} != null)
        @foreach($dataTypeContent->{$row->field} as $image)
            @php
                $url = config('app.url').'/storage/';
                $clearimage = preg_replace( "#^$url#", "", $image);
            @endphp
            <div class="img_settings_container" data-field-name="{{ $row->field }}" style="float:left;padding-right:15px;">
                <a href="#" class="voyager-x remove-multi-image" style="position: absolute;"></a>
                <img src="{{ $image }}" data-file-name="{{ $image }}" data-id="{{ $dataTypeContent->getKey() }}" style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;">
            </div>
        @endforeach
    @endif
@endif
<div class="clearfix"></div>
<input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file" name="{{ $row->field }}[]" multiple="multiple" accept="image/*">
