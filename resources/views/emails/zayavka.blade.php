@component('mail::message')
# Заявка из сайта

Имя: {{ $user['name'] }}<br>
Телефон: {{ $user['phone'] }} <br>
Вопрос: {{ isset($user['question']) ? $user['question'] : '' }}<br>

@endcomponent
