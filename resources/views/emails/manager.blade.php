@component('mail::message')
# ЭЙ У ТЕБЯ НОВЫЙ ЗАКАЗ, БЫСТРО ЗА РАБОТУ!!!

<div class="form-group">
    <h4 style="margin-bottom: 2px;">Заказ #{{$order->id}}</h4>
</div>
<div class="form-group">
    <h4 style="margin-bottom: 2px;">ФИО</h4>
    <p>{{ $order->user ? $order->user->fullname : $order->name }}</p>
</div>
<div class="form-group">
    <h4 style="margin-bottom: 2px;">Номер телефона</h4>
    <p>{{ $order->user ? $order->user->phone : $order->phone }}</p>
</div>
<div class="form-group">
    <h4 style="margin-bottom: 2px;">Дата заказа</h4>
    <p>{{ $order->created_at->format('d.m.Y H:i') }}</p>
</div>

<div class="form-group">
    <h4 style="margin-bottom: 2px;">Способы оплаты</h4>
    <p>{{ $order->paymentMethod ? $order->paymentMethod->name : 'Быстрый заказ' }}</p>
</div>
<div class="form-group">
    <h4 style="margin-bottom: 2px;">Способы доставки</h4>
    <p>{{ $order->deliveryMethod ? $order->deliveryMethod->name : 'Быстрый заказ' }}</p>
</div>
@if ($order->address)
<div class="form-group">
    <h4>Aдрес</h4>
</div>
@component('mail::table')
| Населенный пункт: | {{$order->address->city->name }}|
| :-----------------|:-------------|
| Улица:            | {{$order->address->street }}|
| Дом:              | {{$order->address->house }}|
| Подъезд:          | {{$order->address->entrance }}|
| Этаж:             | {{$order->address->floor }}|
| Квартира:         | {{$order->address->apartment }}|
@endcomponent
@endif
@if ($order->notes)
    <div class="form-group">
        <h4 style="margin-bottom: 2px;">Комментарий</h4>
        <p>{{ $order->notes }}</p>
    </div>
@endif

@component('mail::table')
| Продукт | кол-во | Итого |
| :-----------------|:-------------|:-------------|
@foreach ($order->products as $key => $product)
| {{ $product->product ? $product->product->name : ''}}| × {{ $product->quantity}} | {{ $product->total }} ₸ |
@endforeach
@endcomponent


@endcomponent
