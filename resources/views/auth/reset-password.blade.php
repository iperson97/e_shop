<x-guest-layout>
    <div class="overlay">
        <div class="popup popup-discard">
            <span class="popup__close">
                <svg class="svg-icon icon-close"><use xlink:href="/images/sprite.svg#icon-close"></use></svg>
            </span>

            <p class="popup__title">
                Сброс пароля
            </p>

            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $request->route('token') }}">

                <label class="form__label">
                    <x-jet-input id="email" class="form__field" type="email" name="email" :value="old('email', $request->email)" required placeholder="Email" autofocus />
                </label>
                @error('email')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <label class="form__label">
                    <input id="password" class="form__field" type="password" name="password" required autocomplete="new-password" placeholder="Новый пароль" >
                </label>
                @error('password')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <label class="form__label">

                    <input id="password_confirmation" class="form__field" type="password" name="password_confirmation" placeholder="Повторите пароль" required autocomplete="new-password" >
                </label>
                @error('password_confirmation')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <button class="popup__form-btn" type="submit">
                    Сбросить
                </button>

            </form>

            @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
            @endif

        </div><!--popup-->
    </div><!--overlay-->
</x-guest-layout>
