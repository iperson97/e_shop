$(document).ready(function () {
    $('.catalog__item').hover(function () {
        $(this).prev().addClass('prevHover');
    }, function () {
        $(this).prev().removeClass('prevHover');
    });

    $('.tabs__link').on('click', function (e) {
        e.preventDefault();
        $('.tabs__link').removeClass('active');
        $(this).addClass('active');
        $('.tab').removeClass('active').eq($(this).index()).addClass('active');
        $(window).trigger('scroll');
    });

    // if($('.product__right').index() != -1) {
    //     var productRightTop = $('.product__right').offset().top;
    //     $(window).scroll(function(){
    //         var top = $(this).scrollTop();
    //
    //         if (top > productRightTop && top < productRightTop + $('.product__right').height() - $('.product__slider').height()) {
    //             $('.product__slider').css('top', top - productRightTop);
    //         } else if (top < productRightTop){
    //             $('.product__slider').css('top', '0');
    //         } else {
    //             $('.product__slider').css('top', productRightTop + $('.product__right').height() - $('.product__slider').height() * 1.7);
    //         }
    //     });
    //
    //     $(window).trigger('scroll');
    // }

    const scriptGoogleTag = document.createElement('script')
    scriptGoogleTag.async = true
    scriptGoogleTag.src = 'https://www.googletagmanager.com/gtag/js?id=UA-231689033-1'

    const script = document.createElement('script')

    const code = `window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-231689033-1');`

    try {
        script.appendChild(document.createTextNode(code));
        document.head.appendChild(scriptGoogleTag)
        document.head.appendChild(script);
    } catch (e) {
        script.text = code;
        document.head.appendChild(scriptGoogleTag)
        document.head.appendChild(script);
    }


});
