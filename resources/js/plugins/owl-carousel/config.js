export default {
    sliderBrandConfig: {
        margin: 70,
        loop: true,
        autoWidth: true,
        items: 5,
        responsive: {
            0: {
                autoWidth: false,
                margin: 30,
                items: 2
            },
            500: {
                autoWidth: false,
                margin: 30,
                items: 3
            },
            1024: {
                autoWidth: false,
                items: 4
            },
            1300: {
                items: 5
            }
        }
    },
    sliderMainConfig: {
        smartSpeed: 900,
        loop: true,
        mouseDrag: true,
        items: 1
    }
}
