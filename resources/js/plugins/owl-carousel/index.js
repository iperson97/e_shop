import config from './config';

class OwlCarousel {
    install(Vue){
        Vue.directive('owlcarousel-brands', {
            inserted: function (el) {
                let brandsSlider = Vue.prototype.$owlCarousel(el, config.sliderBrandConfig)
                $('.brands__control-next').click(function() {
                    brandsSlider.trigger('next.owl.carousel');
                });

                $('.brands__control-prev').click(function() {
                    brandsSlider.trigger('prev.owl.carousel');
                });
            },
            update: function (el, binding, vnode) {
                let owl = Vue.prototype.$owlCarousel(el);
                owl.trigger('destroy.owl.carousel');
            },
            componentUpdated: function (el, binding, vnode) {
                Vue.prototype.$owlCarousel(el, config.sliderBrandConfig);
            }
        });


        Vue.directive('owlcarousel-main', {
            inserted: function (el) {
                let mainSlider = Vue.prototype.$owlCarousel(el, config.sliderMainConfig)
                $('.main__control-next').click(function() {
                    mainSlider.trigger('next.owl.carousel');
                });

                $('.main__control-prev').click(function() {
                    mainSlider.trigger('prev.owl.carousel');
                });
            },
            update: function (el, binding, vnode) {
                let owl = Vue.prototype.$owlCarousel(el);
                owl.trigger('destroy.owl.carousel');
            },
            componentUpdated: function (el, binding, vnode) {
                Vue.prototype.$owlCarousel(el, config.sliderMainConfig);
            }
        });


        Vue.directive('owlcarousel-product-slides', {
            inserted: function (el) {
                let productSlider = Vue.prototype.$owlCarousel(el, {})
            },
            update: function (el, binding, vnode) {
                let owl = Vue.prototype.$owlCarousel(el);
                owl.trigger('destroy.owl.carousel');
            },
            componentUpdated: function (el, binding, vnode) {
                let productSlider = Vue.prototype.$owlCarousel(el, {
                    loop:true,
                    items:1
                })
                productSlider.on('translated.owl.carousel', function (e) {
                    $('.product__preview').removeClass('active');
                    $('.product__preview' + '[data-index= ' + $('.product__slides .owl-item.active').find('.product__slide').data('index') + ']').addClass('active');
                });
                $('.product__previews').on('click', '.product__preview', function() {
                    $('.product__preview').removeClass('active');
                    $(this).addClass('active');
                    productSlider.trigger('to.owl.carousel', $(this).data('index'));
                });
            }
        });


        Vue.directive('owlcarousel-product-previews', {
            inserted: function (el) {
                let productSlider = Vue.prototype.$owlCarousel(el, {})
            },
            update: function (el, binding, vnode) {
                let owl = Vue.prototype.$owlCarousel(el);
                owl.trigger('destroy.owl.carousel');
            },
            componentUpdated: function (el, binding, vnode) {
                let productPreviews = Vue.prototype.$owlCarousel(el, {
                    margin: 4,
                    loop: false,
                    items: 4
                })

                $('.product__previews-next').click(function() {
                    productPreviews.trigger('next.owl.carousel');
                });

                $('.product__previews-prev').click(function() {
                    productPreviews.trigger('prev.owl.carousel');
                });
            }
        });

        Vue.prototype.$owlCarousel = (el, sliderconfig) => {
            return $(el).owlCarousel(sliderconfig);
        }
    }

}

export default new OwlCarousel()
