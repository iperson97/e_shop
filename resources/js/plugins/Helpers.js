import moment from 'moment';
const numeral = require('numeral');
moment.locale('ru');

export default {
    install(Vue, { page }) {

        Vue.filter('priceFilter', value => numeral(value).format('0,0.[00]').replace(/,/g, ' '));
        Vue.filter('currencyNum', value => numeral(value).format('(0a)'));

        Vue.filter('dateFormat', function (value, format) {
            if (value) {
                return moment(String(value)).format(format || "DD.MM.YYYY")
            }
        });

        Vue.filter('dateFormatForHuman', function (value, format) {
            if (value) {
                return moment(String(value)).format(format || "DD MMMM YYYY")
            }
        });

        Vue.filter('timeFormat', function (value, format) {
            if (value) {
                return moment(String(value)).format(format || "H:mm")
            }
        });
        Vue.filter('datetimeFormat', function (value, format) {
            if (value) {
                return moment(String(value)).format(format || "DD.MM.YYYY H:mm")
            }
        });

        Vue.prototype.laraPaginate = (data) => {
            let pagination = {}
            let paginate = Object.keys(data).filter(key => {
                return data[key] != data.data
            }).map(key => {
                pagination[key] = data[key]
            })

            return pagination
        },

        Vue.prototype.getIndex = (array, key, value) => {
            return array.findIndex(i => i[key] == value)
        },

        Vue.prototype.$objectPropToArray = (array, key) => {
            let result = [];
            array.map(item => {
                result.push(item[key])
            });
            return result;
        },

        Vue.prototype.laravelValidation = (err) => {
            if (err.response) {
                if (err.response.status === 422) {
                    return err.response.data.errors;
                }
            }
            return {};
        },

        Vue.prototype.$toastrError = (text, title) => {
            window.toastr.options.progressBar = true;
            window.toastr.options.closeButton = true;
            window.toastr.options.timeOut = 10000;
            window.toastr.options.extendedTimeOut = 20000;
            window.toastr.options.preventDuplicates = true;
            window.toastr.error(text, title)
        },

        Vue.prototype.$toastrSuccess = (text, title) => {
            window.toastr.options.progressBar = true;
            window.toastr.options.closeButton = true;
            window.toastr.options.timeOut = 10000;
            window.toastr.options.extendedTimeOut = 20000;
            window.toastr.options.preventDuplicates = true;
            window.toastr.success(text, title)
        },

        Vue.prototype.$scrollTo = (selector) => {
            let selectedProductsComponent = document.querySelector(selector);
            if (selectedProductsComponent) {
                selectedProductsComponent.scrollIntoView({ behavior: 'smooth'});
            } else {
                console.log('not found selector');
            }
        },

        Vue.prototype.$loginCheckModal = (url, modal) => {
            if (page.props.user) {
                window.location = url;
            } else {
                modal.show('login-modal');
            }
        },

        Vue.prototype.$phonemask = {
            mask: '+#(###) ###-##-##',
            autoUnmask: true,
            showMaskOnHover: false,
        }
    }
};
