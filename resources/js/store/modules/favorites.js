export default {
    state: {
        favorite_items: [],
        favorite_items_count: 0,
    },
    mutations: {
        SET_FAVORITE_COUNT(state, { count }) {
            state.favorite_items_count = count;
        },
    },
    actions: {
        async GET_FAVORITE_PRODUCTS_COUNT({ commit }) {
            try {
                const { data } = await window.axios.get('/favorites/count');
                commit('SET_FAVORITE_COUNT', data);
            } catch (err) {
                throw err;
            }
        },
        async ADD_OR_REMOVE_FAVORITE_PRODUCT({ commit }, id) {
            try {
                const { data } = await window.axios.post('/favorites/add-or-remove/' + id);
            } catch (err) {
                throw err;
            }
        },
        async REMOVE_FAVORITE_PRODUCT({ commit }, id) {
            try {
                const { data } = await window.axios.delete('/favorites/remove/' + id);
            } catch (err) {
                throw err;
            }
        },
    },
    getters: {
        favorite_items(state) {
            return state.favorite_items;
        },
        favorite_items_count(state) {
            return state.favorite_items_count;
        },
    },
};
