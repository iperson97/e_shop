export default {
    state: {
        orders: [],
    },
    mutations: {
        CHECKOUT_ORDER(state, payload) {
            if (payload === 201) {
                window.location.href = "/home";
            }
        },
    },
    actions: {
        async CHECKOUT_ORDER({ commit }, payload) {
            try {
                const { data, status } = await window.axios.post('/checkout', payload);
                commit('CHECKOUT_ORDER', status);
            } catch (err) {
                throw err;
            }
        },
    },
    getters: {
        orders(state) {
            return state.orders;
        },
    },
};
