export default {
    state: {
        items_comparison: [],
        items_comparison_count: 0,
    },
    mutations: {
        SET_COMPARISON_COUNT(state, { count }) {
            state.items_comparison_count = count;
        },
    },
    actions: {
        async GET_PRODUCTS_COMPARISON_COUNT({ commit }) {
            try {
                const { data } = await window.axios.get('/comparison/count');
                commit('SET_COMPARISON_COUNT', data);
            } catch (err) {
                throw err;
            }
        },
        async ADD_PRODUCT_COMPARISON({ commit }, id) {
            try {
                const { data } = await window.axios.post('/comparison/add/' + id);
            } catch (err) {
                throw err;
            }
        },
        async REMOVE_PRODUCT_COMPARISON({ commit }, id) {
            try {
                const { data } = await window.axios.delete('/comparison/remove/' + id);
            } catch (err) {
                throw err;
            }
        },
    },
    getters: {
        items_comparison(state) {
            return state.items_comparison;
        },
        items_comparison_count(state) {
            return state.items_comparison_count;
        },
    },
};
