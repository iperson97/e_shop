export default {
    state: {
        cart_item: {},
        cart_items: [],
        cart_products_count: 0,
        cart_detail: {},
    },
    mutations: {
        SET_CART(state, payload) {
            state.cart_items = payload.items;
        },
        SET_CART_ITEM(state, { product }) {
            state.cart_item = product;
        },
        SET_PRODUCTS_COUNT(state, { count }) {
            state.cart_products_count = count;
        },
        SET_CART_DETAIL(state, { details }) {
            state.cart_detail = details;
        },
    },
    actions: {
        async GET_CART_ITEMS({ commit }) {
            try {
                const { data } = await window.axios.get('/cart', {
                    params: {
                        limit: 10
                    }
                });
                commit('SET_CART', data);
                commit('SET_PRODUCTS_COUNT', data);
            } catch (err) {
                throw err;
            }
        },
        async GET_CART_COUNT({ commit }) {
            try {
                const { data } = await window.axios.get('/cart/count');
                commit('SET_PRODUCTS_COUNT', data);
            } catch (err) {
                throw err;
            }
        },
        async ADD_CART({ commit }, { id, quantity }) {
            try {
                const { data } = await window.axios.post('/cart', { id, quantity });
                commit('SET_CART_ITEM', data);
            } catch (err) {
                throw err;
            }
        },
        async UPDATE_CART_ITEM_QUANTITY({ commit }, payload) {
            try {
                const { data } = await window.axios.put('/cart/' + payload.id, payload);
            } catch (err) {
                throw err;
            }
        },
        async GET_CART_DETAIL({ commit }, payload) {
            try {
                const { data } = await window.axios.get('/cart/details');
                commit('SET_CART_DETAIL', data);
            } catch (err) {
                throw err;
            }
        },
        async REMOVE_CART_PRODUCT({ commit }, payload) {
            try {
                const { data } = await window.axios.delete('/cart/' + payload);
            } catch (err) {
                throw err;
            }
        },
    },
    getters: {
        cart_items(state) {
            return state.cart_items;
        },
        cart_item(state) {
            return state.cart_item;
        },
        cart_products_count(state) {
            return state.cart_products_count;
        },
        cart_detail(state) {
            return state.cart_detail;
        },
    },
};
