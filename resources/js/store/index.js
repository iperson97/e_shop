import Vue from 'vue';
import Vuex from 'vuex';
import cart from './modules/cart';
import favorites from './modules/favorites';
import сomparison from './modules/сomparison';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
      cart,
      favorites,
      сomparison,
  },
});
