require('./bootstrap');
require('./scripts');

import Vue from 'vue';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';
import VModal from 'vue-js-modal';
import VueStarRating from 'vue-star-rating';const VueInputMask = require('vue-inputmask').default;
import SlideUpDown from 'vue-slide-up-down'

import store from './store';

import Helpers from './plugins/Helpers';
import LaravelValidator from './plugins/laravel-validator';
import InteractsWithErrorBags from './Mixins/InteractsWithErrorBags';
import TitleMixin from './Mixins/titleMixin';
import OwlCarousel from './plugins/owl-carousel';
const app = document.getElementById('app');

Vue.mixin(TitleMixin);
Vue.mixin({ methods: { route } });
Vue.mixin(InteractsWithErrorBags);
Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);
Vue.use(VModal);
Vue.use(VueInputMask);


Vue.use(Helpers, { page: JSON.parse(app.dataset.page)});
Vue.use(LaravelValidator);
Vue.use(OwlCarousel);

Vue.component('star-rating', VueStarRating);
Vue.component('shop-layout', require('./Layouts/ShopLayout.vue').default);
Vue.component('shop-recommendations', require('./Eshop/Containers/Recommendations.vue').default);
Vue.component('shop-new-items', require('./Eshop/Containers/NewItems.vue').default);
Vue.component('shop-category-list', require('./Eshop/Containers/CategoryList.vue').default);
Vue.component('no-ui-slider', require('./Eshop/Components/noUiSlider.vue').default);

Vue.component('slide-up-down', SlideUpDown)



new Vue({
    store,
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app);
