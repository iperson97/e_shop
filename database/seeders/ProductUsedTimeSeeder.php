<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\ProductUsedTime;

class ProductUsedTimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Менее месяца',
            ],
            [
                'name' => 'Полгода',
            ],
            [
                'name' => 'Более года',
            ],
        ];

        ProductUsedTime::insert($categories);
    }
}
