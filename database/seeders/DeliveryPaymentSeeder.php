<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DeliveryMethod;

class DeliveryPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = [1,2,3,4,5];
        $methods = [
            1 => [1,2],
            2 => [1,2],
            3 => [2,3,4],
            4 => [2],
            5 => [2,3,4],
        ];
        $deliverymethods = DeliveryMethod::whereIn('id', $ids)->get();
        foreach ($deliverymethods as $key => $value) {
            $value->paymentMethods()->sync($methods[$value->id]);
        }
    }
}
