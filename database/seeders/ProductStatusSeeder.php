<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductStatus;

class ProductStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      if (ProductStatus::count() == 0) {
          $statuses = [
              [
                  'name' => 'В наличии',
                  'color' => '#0CB906',
              ],
              [
                  'name' => 'На складе',
                  'color' => '#2b56b6',
              ],
              [
                  'name' => 'На заказ',
                  'color' => '#0CB906',
              ],
              [
                  'name' => 'Нет в наличии',
                  'color' => '#ff0000',
              ],
          ];

          ProductStatus::insert($statuses);
      }
    }
}
