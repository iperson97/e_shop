<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\DeliveryMethod;

class DeliveryMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DeliveryMethod::count() == 0) {
            $deliverymethods = [
                [
                    'name' => 'Доставка',
                ],
                [
                    'name' => 'Доставка с установкой',
                ],
                [
                    'name' => 'Самовывоз',
                ],
                [
                    'name' => 'Доставка через курьерскую службу, уточнять у менеджера',
                ],
                [
                    'name' => 'Cамовывоз с Алматы',
                ],
            ];

            DeliveryMethod::insert($deliverymethods);
        }
    }
}
