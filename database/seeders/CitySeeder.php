<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (City::count() == 0) {
            $cities = [
                [
                    'name' => 'АЛМАТЫ',
                ],
                [
                    'name' => 'ТАЛДЫКОРГАН',
                ],
                [
                    'name' => 'КАПШАГАЙ',
                ],
                [
                    'name' => 'КАРАГАНДА',
                ],
                [
                    'name' => 'АТЫРАУ',
                ],
                [
                    'name' => 'АКТАУ',
                ],
                [
                    'name' => 'ПЕТРОПАВЛОВСК',
                ],
                [
                    'name' => 'ТАРАЗ',
                ],
                [
                    'name' => 'АКТОБЕ',
                ],
            ];

            City::insert($cities);
        }
    }
}
