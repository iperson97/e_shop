<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'ASUS',
            ],
            [
                'name' => 'Lenovo',
            ],
            [
                'name' => 'HP',
            ],
            [
                'name' => 'Dell',
            ],
            [
                'name' => 'Samsung',
            ],
            [
                'name' => 'Huawei',
            ],
            [
                'name' => 'Apple',
            ],
            [
                'name' => 'Google',
            ],
            [
                'name' => 'Xiaomi',
            ],
            [
                'name' => 'Redmi',
            ],
        ];

        Brand::insert($categories);
    }
}
