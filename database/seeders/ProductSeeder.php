<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::get();
        foreach ($categories as $key => $category) {
            $products = Product::factory()
                                ->count(5)
                                ->state([
                                    'name' => Str::slug($category->name, '-'),
                                    'isnew' => $category->parent_id ? 1 : 0,
                                    'isrecommend' => ($category->parent_id == null) ? 1 : 0,
                                ])
                                ->make();
            $category->products()->createMany($products->toArray());
        }
    }
}
