<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FilterValue;
use App\Models\Product;

class FilterValueProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = [15, 14, 8, 198, 199];
        $prodArt = [
            15 => [33545,35659,33868],
            14 => [35293,33523,32148],
            8 => [33781,34776,29984,28404],
            198 => [29060,22127,27796],
            199 => [29060,25149,29056],
        ];
        $filterValues = FilterValue::whereIn('id', $ids)->get();
        foreach ($filterValues as $key => $value) {
            $products = Product::whereIn('article', $prodArt[$value->id])->get()->pluck('id')->all();
            $value->products()->sync($products);
        }
    }
}
