<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Filter;
use App\Models\FilterValue;

class FilterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filters = [
            [
                'name' => 'Для ноутбука',
                'category_id' => 12,
                'values' => ['ddr3', 'ddr4'],
            ],
            [
                'name' => 'Для компьютера',
                'category_id' => 12,
                'values' => ['ddr3', 'ddr4'],
            ],
            [
                'name' => 'Тип процессора',
                'category_id' => 11,
                'values' => [
                    'intel Celeron',
                    'intel pentium',
                    'intel i3',
                    'intel i5',
                    'intel i7',
                    'intel i9',
                    'amd Athlon',
                    'amd Ryzen Threadripper',
                    'amd Ryzen 3',
                    'amd Ryzen 5',
                    'amd Ryzen 7',
                ],
            ],
            [
                'name' => 'Видеокарты',
                'category_id' => 13,
                'values' => ['Nvidia','AMD'],
            ],
            [
                'name' => 'Объем памяти',
                'category_id' => 13,
                'values' => [
                    '1 ГБ',
                    '2 ГБ',
                    '3 ГБ',
                    '4 ГБ',
                    '6 ГБ',
                    '8 ГБ',
                    '10 ГБ',
                    '11 ГБ',
                    '16 ГБ',
                    '24 ГБ',
                ],
            ],
            [
                'name' => 'Жесткие диски',
                'category_id' => 14,
                'values' => [
                    '500 Гб',
                    '1000 Гб',
                    '2000 Гб',
                    '3000 Гб',
                    '4000 Гб',
                    '6000 Гб',
                    '8000 Гб',
                    '10000 Гб',
                    '12000 Гб',
                    '14000 Гб',
                    '16000 Гб',
                ],
            ],
            [
                'name' => 'Форм Фактор',
                'category_id' => 14,
                'values' => [
                    '2.5',
                    '3.5',
                ]
            ],
            [
                'name' => 'Чипсеты',
                'category_id' => 15,
                'values' => [
                    'A320',
                    'A520',
                    'B350',
                    'B450',
                    'B550',
                    'TRX40',
                    'X470',
                    'X570',
                    'B360',
                    'B365',
                    'B460',
                    'Z390',
                    'Z490',
                ],
            ],
            [
                'name' => 'Блоки Питания',
                'category_id' => 16,
                'values' => [
                    '350W',
                    '400W',
                    '450W',
                    '500W',
                    '550W',
                    '600W',
                    '650W',
                    '700W',
                    '750W',
                    '800W',
                    '850W',
                    '1000W',
                    '1050W',
                    '1200W',
                    '1300W',
                ]
            ],
            [
                'name' => 'Тип сокета',
                'category_id' => 17,
                'values' => [
                    '1150',
                    '1151',
                    '1155',
                    '1156',
                    '1200',
                    '1366',
                    '2011',
                    '2011-3',
                    '775',
                    '2066',
                    'AM',
                    'AM2+',
                    'AM3',
                    'AM3+',
                    'AM4',
                    'FM1',
                    'FM2',
                    'FM2+',
                ],
            ],
            [
                'name' => 'Интерфейс',
                'category_id' => 18,
                'values' => [
                    'PCIe',
                    'Sata',
                    'm.Sata',
                    'M.2 Sata',
                    'Pci-e m.2',
                ],
            ],
            [
                'name' => 'Емкость диска',
                'category_id' => 18,
                'values' => [
                    '120 Гб',
                    '128 Гб',
                    '240 Гб',
                    '250 Гб',
                    '256 Гб',
                    '480 Гб',
                    '500 Гб ',
                    '512 Гб',
                    '960 Гб',
                    '1000 Гб',
                    '2000 Гб',
                    '4000 Гб',
                    '8000 Гб',
                ],
            ],
            [
                'name' => 'Разрешение',
                'category_id' => 20,
                'values' => [
                    '240p',
                    '360p',
                    '480p',
                    '720p',
                    '1080p',
                    '1440p',
                    '2160p',
                ],
            ],
            [
                'name' => 'Микрофон',
                'category_id' => 20,
                'values' => [
                    'Есть',
                    'Нет',
                ],
            ],
            [
                'name' => 'Диагональ',
                'category_id' => 21,
                'values' => [
                    '18.5',
                    '19',
                    '19.5',
                    '20',
                    '21',
                    '21.5',
                    '22',
                    '23',
                    '23.5',
                    '23.6',
                    '23.8',
                    '24',
                    '25',
                    '27',
                    '29',
                    '31.5',
                    '32',
                    '34',
                    '35',
                    '37.5',
                    '38.5',
                    '42.5',
                    '43',
                    '43.4',
                    '49',
                    '55',
                ],
            ],
            [
                'name' => 'Разрешение',
                'category_id' => 21,
                'values' => [
                    '1280х1024',
                    '1366х768',
                    '1600х900',
                    '1680х1050',
                    '1920х1200',
                    '1920х1080',
                    '2560х1080',
                    '2560х1440',
                    '3440х1440',
                    '3840х1200',
                    '3840х1600',
                    '3840х2160',
                    '5120х1440',
                ],
            ],
            [
                'name' => 'Интерфейс',
                'category_id' => 21,
                'values' => [
                    'VGA',
                    'DVI',
                    'USB',
                    'Mini DP',
                    'DP',
                    'HDMI',
                    'AUX',
                    'USB Type-C',
                ],
            ],
            [
                'name' => 'Тип матрицы',
                'category_id' => 21,
                'values' => [
                    'IPS',
                    'MVA',
                    'TN',
                    'VA',
                    'PLS',
                ],
            ],
            [
                'name' => 'Частота кадров',
                'category_id' => 21,
                'values' => [
                    '60 Гц',
                    '75 Гц',
                    '144 Гц',
                    '165 Гц',
                    '200 Гц',
                    '240 Гц',
                    '280 Гц',
                    '320 Гц',
                ],
            ],
            [
                'name' => 'Изогнутый экран',
                'category_id' => 21,
                'values' => [
                    'Есть',
                    'Нет',
                ],
            ],
            [
                'name' => 'Тип подключения',
                'category_id' => 22,
                'values' => [
                    'Проводное',
                    'Беспроводное',
                ],
            ],
            [
                'name' => 'Тип клавиатуры',
                'category_id' => 22,
                'values' => [
                    'Механика',
                    'Мембранная',
                    'Ножничная',
                    'Механическо-мембранная',
                ],
            ],
            [
                'name' => 'Тип механических клавиш',
                'category_id' => 22,
                'values' => [
                    'Blue',
                    'Red',
                    'Green',
                    'Orange',
                    'Brown',
                    'Silver',
                    'Black',
                ],
            ],
            [
                'name' => 'Тип подключения',
                'category_id' => 23,
                'values' => [
                    'Проводное',
                    'Беспроводное',
                ],
            ],
            [
                'name' => 'DPI',
                'category_id' => 23,
                'type' => 'slider',
                'data' => [
                    'min' => 16,
                    'min' => 100,
                    'max' => 24000,
                ]
            ],
            [
                'name' => 'Внешние HDD',
                'category_id' => 24,
                'type' => 'slider',
                'data' => [
                    'min' => 500,
                    'step' => 100,
                    'max' => 10000,
                ]
            ],
            [
                'name' => 'USB',
                'category_id' => 24,
                'type' => 'slider',
                'data' => [
                    'min' => 8,
                    'step' => 8,
                    'max' => 512,
                ]
            ],
            [
                'name' => 'Тип флешки',
                'category_id' => 24,
                'values' => [
                    'USB-A',
                    'USB-C',
                ],
            ],
            [
                'name' => 'Карты Памяти',
                'category_id' => 24,
                'type' => 'slider',
                'data' => [
                    'min' => 8,
                    'step' => 8,
                    'max' => 512,
                ]
            ],
            [
                'name' => 'Акустическая система',
                'category_id' => 26,
                'values' => [
                    '2.0',
                    '2.1',
                    '5.1',
                ],
            ],
            [
                'name' => 'Процессор',
                'category_id' => 4,
                'values' => [
                    'AMD Ryzen 3',
                    'AMD Ryzen 5',
                    'AMD Ryzen 7',
                    'Intel Core i3',
                    'Intel Core i5',
                    'Intel Core i7',
                    'Intel Celeron',
                    'Intel Pentium',
                ],
            ],
            [
                'name' => 'Оперативная память',
                'category_id' => 4,
                'values' => [
                    '2 Гб',
                    '4 Гб',
                    '8 Гб',
                    '12 Гб',
                    '16 Гб',
                    '32 Гб',
                    '64 Гб',

                ],
            ],
            [
                'name' => 'Экран',
                'category_id' => 4,
                'values' => [
                    '13',
                    '13.3',
                    '14.0',
                    '15.6',
                    '17.0',
                    '17.3',

                ],
            ],
        ];

        foreach ($filters as $key => $value) {
            $filter = Filter::updateOrCreate(
                [
                    'name' => $value['name'],
                    'category_id' => $value['category_id'] ?? null
                ],
                [
                    'type' => $value['type'] ?? 'checkbox',
                    'data' => $value['data'] ?? []
                ]
            );
            $filter->categories()->sync([$value['category_id']]);
            $filterValues = [];
            if (isset($value['values'] )) {
                foreach ($value['values'] as $key => $filterValue) {
                    FilterValue::firstOrCreate([
                        'name' => $filterValue,
                        'filter_id' => $filter->id
                    ]);
                }
            }

        }

    }
}
