<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\City;

class CityDeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = City::get();
        foreach ($cities as $key => $value) {
            if ($value->id == 1) {
                $methods = [1,2,3];
            } else {
                $methods = [4,5];
            }
            $value->deliveryMethods()->sync($methods);
        }
    }
}
