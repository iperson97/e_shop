<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Компьютеры',
                'alstyle_ids' => null,
                'slug' => Str::slug('Компьютеры', '-'),
                'image' => '/images/categories/category-1.png',
            ],
            [
                'name' => 'Комплектующие для ПК',
                'alstyle_ids' => null,
                'slug' => Str::slug('Комплектующие', '-'),
                'image' => '/images/categories/category-2.png',
            ],
            [
                'name' => 'Периферия',
                'alstyle_ids' => null,
                'slug' => Str::slug('Периферия', '-'),
                'image' => '/images/categories/category-3.png',
            ],
            [
                'name' => 'Ноутбуки',
                'alstyle_ids' => '[3661]',
                'slug' => Str::slug('Ноутбуки', '-'),
                'image' => '/images/categories/category-4.png',
            ],
            [
                'name' => 'Компьютерная мебель',
                'alstyle_ids' => null,
                'slug' => Str::slug('Компьютерная мебель', '-'),
                'image' => '/images/categories/category-1.png',
            ],
            [
                'name' => 'Уценённый товар',
                'alstyle_ids' => null,
                'slug' => Str::slug('Уценённый товар', '-'),
                'image' => '/images/categories/category-1.png',
            ],
        ];

        Category::insert($categories);

        $categories = [
            //Компьютеры
            [
                'parent_id' => 1,
                'alstyle_ids' => null,
                'name' => 'Игровые',
                'slug' => Str::slug('Игровые', '-'),
            ],
            [
                'parent_id' => 1,
                'alstyle_ids' => null,
                'name' => 'Офисные',
                'slug' => Str::slug('Офисные', '-'),
            ],
            [
                'parent_id' => 1,
                'alstyle_ids' => null,
                'name' => 'Эксклюзивные',
                'slug' => Str::slug('Эксклюзивные', '-'),
            ],
            [
                'parent_id' => 1,
                'alstyle_ids' => null,
                'name' => 'Дизайнерам',
                'slug' => Str::slug('Дизайнерам', '-'),
            ],

            //Комплектующие для ПК
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3490]',
                'name' => 'Корпусное охлаждение',
                'slug' => Str::slug('Корпусное охлаждение', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3593]',
                'name' => 'Процессоры',
                'slug' => Str::slug('Процессоры', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[4965]',
                'name' => 'Оперативная память',
                'slug' => Str::slug('Оперативная память', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3587]',
                'name' => 'Видеокарты',
                'slug' => Str::slug('Видеокарты', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3589]',
                'name' => 'Жесткие диски (HDD)',
                'slug' => Str::slug('Жесткие диски (HDD)', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3590]',
                'name' => 'Материнские платы',
                'slug' => Str::slug('Материнские платы', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3615]',
                'name' => 'Блоки питания',
                'slug' => Str::slug('Блоки питания', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3493]',
                'name' => 'Кулеры для процессоров',
                'slug' => Str::slug('Кулеры для процессоров', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3594]',
                'name' => 'Твердотельные накопители (SSD)',
                'slug' => Str::slug('Твердотельные накопители (SSD)', '-'),
            ],
            [
                'parent_id' => 2,
                'alstyle_ids' => '[3616]',
                'name' => 'Корпуса',
                'slug' => Str::slug('Корпуса', '-'),
            ],

            //Перифирия
            [
                'parent_id' => 3,
                'alstyle_ids' => '[3438]',
                'name' => 'Вебкамеры',
                'slug' => Str::slug('Вебкамеры', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => '[5649]',
                'name' => 'Мониторы',
                'slug' => Str::slug('Мониторы', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => '[3783,3775,3778]',
                'name' => 'Клавиатуры',
                'slug' => Str::slug('Клавиатуры', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => '[3785,3777,3782,3779]',
                'name' => 'Мыши',
                'slug' => Str::slug('Мыши', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => null,
                'name' => 'Носители информации',
                'slug' => Str::slug('Носители информации', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => '[3786,3781,3780]',
                'name' => 'Коврики',
                'slug' => Str::slug('Коврики', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => '[3466]',
                'name' => 'Акустика',
                'slug' => Str::slug('Акустика', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => null,
                'name' => 'Кабели',
                'slug' => Str::slug('Кабели', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => null,
                'name' => 'Принтеры и мфу',
                'slug' => Str::slug('Принтеры и мфу', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => null,
                'name' => 'Аксессуары',
                'slug' => Str::slug('Аксессуары', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => null,
                'name' => 'Ибп и стабилизаторы',
                'slug' => Str::slug('Ибп и стабилизаторы', '-'),
            ],
            [
                'parent_id' => 3,
                'alstyle_ids' => null,
                'name' => 'Сетевое оборудование',
                'slug' => Str::slug('Сетевое оборудование', '-'),
            ],

            //Ноутбуки
            [
                'parent_id' => 4,
                'alstyle_ids' => null,
                'name' => 'Игровые ноутбуки',
                'slug' => Str::slug('игровые ноутбуки', '-'),
            ],
            [
                'parent_id' => 4,
                'alstyle_ids' => null,
                'name' => 'Офисные ноутбуки',
                'slug' => Str::slug('Офисные ноутбуки', '-'),
            ],
            [
                'parent_id' => 4,
                'alstyle_ids' => null,
                'name' => 'Ультрабуки',
                'slug' => Str::slug('Ультрабуки', '-'),
            ],

            //Компьютерная мебель
            [
                'parent_id' => 5,
                'alstyle_ids' => null,
                'name' => 'Компьютерные столы',
                'slug' => Str::slug('Компьютерные столы', '-'),
            ],
            [
                'parent_id' => 5,
                'alstyle_ids' => null,
                'name' => 'Компьютерные кресла',
                'slug' => Str::slug('Компьютерные кресла', '-'),
            ],
        ];

        Category::insert($categories);
    }
}
