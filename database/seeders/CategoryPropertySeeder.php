<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryPropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = [10,11,12,13,14,16,15,17,18,19,36];
        $properties = [
            10 => [33,34,36,37,41,48,49,42,43,44,60],
            11 => [66,68,67,63,62,64,69],
            12 => [71,72,74,75,76,70],
            13 => [76,80,81,83,84,90,2],
            14 => [30,94,76,97,96,98,96],
            16 => [109,107,110,111,18],
            15 => [66,70,8,100,104,102],
            17 => [33,139,13,138,34,78,140,37,36,142,145],
            18 => [94,148,76,149,84],
            19 => [151,156,152,150,158,164,165],
            36 => [13,356,355,358,360,361,138],
        ];
        $categories = Category::whereIn('id', $ids)->get();
        foreach ($categories as $key => $value) {
            $value->properties()->sync($properties[$value->id]);
            $this->command->info($value->name);
        }
    }
}
