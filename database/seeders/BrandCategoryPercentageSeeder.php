<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BrandCategoryPercentageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [45];
        $categories = [
            45 => [39 => ['percentage_markup' => 0]],
        ];
        $brands = \App\Models\Brand::whereIn('id', $brands)->get();
        foreach ($brands as $key => $value) {
            $value->categories()->sync($categories[$value->id]);
            $this->command->info($value->categories);
        }
    }
}
