<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentMethod;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (PaymentMethod::count() == 0) {
            $methods = [
                [
                    'name' => 'Наличными курьеру',
                ],
                [
                    'name' => 'Банковской картой',
                ],
            ];

            PaymentMethod::insert($methods);
        }

    }
}
