<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')
                    ->unsigned()
                    ->nullable();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->bigInteger('status_id')
                    ->unsigned()
                    ->nullable();

            $table->foreign('status_id')
                    ->references('id')
                    ->on('order_statuses')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->bigInteger('payment_method_id')
                    ->unsigned()
                    ->nullable();

            $table->foreign('payment_method_id')
                    ->references('id')
                    ->on('payment_methods')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

            $table->bigInteger('delivery_method_id')
                    ->unsigned()
                    ->nullable();

            $table->foreign('delivery_method_id')
                    ->references('id')
                    ->on('delivery_methods')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

            $table->float('total', 20, 2)->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
