<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')
                    ->unsigned()
                    ->nullable();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->bigInteger('used_time_id')
                    ->unsigned()
                    ->nullable();

            $table->foreign('used_time_id')
                    ->references('id')
                    ->on('product_used_times')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

            $table->bigInteger('product_id')
                    ->unsigned()
                    ->nullable();

            $table->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('rating')->default(1);
            $table->text('advantages')->nullable();
            $table->text('disadvantages')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
    }
}
