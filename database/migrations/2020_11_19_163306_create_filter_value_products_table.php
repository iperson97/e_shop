<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilterValueProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_value_products', function (Blueprint $table) {
            $table->bigInteger('filter_value_id')
                    ->unsigned()
                    ->nullable();
            $table->foreign('filter_value_id')
                    ->references('id')
                    ->on('filter_values')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->bigInteger('product_id')
                    ->unsigned()
                    ->nullable();
            $table->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->primary(['filter_value_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filter_value_products');
    }
}
