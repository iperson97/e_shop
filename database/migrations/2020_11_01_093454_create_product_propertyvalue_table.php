<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPropertyvalueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_propertyvalue', function (Blueprint $table) {
            $table->bigInteger('product_id')
                    ->unsigned()
                    ->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');

            $table->bigInteger('property_value_id')
                    ->unsigned()
                    ->nullable();
            $table->foreign('property_value_id')->references('id')->on('property_values')->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['product_id', 'property_value_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_property_values');
    }
}
