<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_values', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('filter_id')
                    ->unsigned()
                    ->nullable();
            $table->foreign('filter_id')
                    ->references('id')
                    ->on('filters')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                    
            $table->text('name');
            $table->integer('sort_order')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filter_values');
    }
}
