<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('full_name')->nullable();
            $table->string('slug')->unique();
            $table->string('article')->nullable();
            $table->string('article_pn')->comment('Артикул (Part Number)')->nullable();
            $table->json('images')->nullable();
            $table->text('description')->nullable();
            $table->bigInteger('sort')->nullable();
            $table->bigInteger('price')->comment('цена')->default(0);
            $table->bigInteger('old_price')->comment('старая цена')->default(0);
            $table->bigInteger('price1')->comment('Цена дилерская')->default(0);
            $table->string('left_quantity')->default(0);
            $table->boolean('isnew')->default(0);
            $table->boolean('ishit')->default(0);
            $table->boolean('ispromo')->default(0);
            $table->boolean('isrecommend')->default(0);
            $table->string('url')->nullable();
            $table->json('properties')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
