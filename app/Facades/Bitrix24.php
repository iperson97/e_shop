<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Bitrix24 extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'bitrix24';
    }
}
