<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Category, Brand, Property, PropertyValue, City, DeliveryMethod};

class FilterController extends Controller
{
    public function categoryBrands(Category $category)
    {
        $categoryIds = $category->parent_id ? [$category->id] : $category->children->pluck('id');

        $brands = Brand::whereHas('products', function ($query) use ($categoryIds) {
            $query->whereIn('category_id', $categoryIds);
        }, '>=', 1)->withCount(['products' => function ($query) use ($categoryIds) {
            $query->whereIn('category_id', $categoryIds);
        }])->get();

        return response()->json([
            'brands' => $brands,
        ]);
    }

    public function categoryProperties(Category $category)
    {
        $categoryIds = $category->parent_id ? [$category->id] : $category->children->pluck('id');

        $properties = $category->parent_id ? $category->properties()->with(['values' => function ($query) use ($categoryIds) {
            $query->whereHas('products', function ($query) use ($categoryIds) {
                $query->whereIn('category_id', $categoryIds);
            }, '>=', 1)->orderBy('name');
        }])->whereHas('values', function ($query) use ($categoryIds) {
            $query->whereHas('products', function ($query) use ($categoryIds) {
                $query->whereIn('category_id', $categoryIds);
            }, '>=', 1);
        }, '>=', 1)->get(): [];

        return response()->json([
            'properties' => $properties,
        ]);
    }


    public function categoryFilters(Category $category)
    {
        $filters = $category->filters()->with('values')->get();
        return response()->json([
            'options' => $filters,
        ]);
    }

    public function cityDeliveryMethods(City $city)
    {
        $deliveryMethods = $city->deliveryMethods()->with('paymentMethods')->get();

        return response()->json([
            'delivery_methods' => $deliveryMethods,
        ]);
    }

    public function deliveryPaymentMethods(DeliveryMethod $delivery)
    {
        $paymentMethods = $delivery->paymentMethods;

        return response()->json([
            'payment_methods' => $paymentMethods,
        ]);
    }
}
