<?php

namespace App\Http\Controllers;

use App\Services\UpdateService;
use Illuminate\Support\Facades\Http;
use App\Models\Product;
use App\Models\Good;

class updateDbController extends Controller
{

    public function updateGoods(){
        return \App\Services\UpdateService::updateGoods();
    }

    public function updateCategories(){
        return \App\Services\UpdateService::updateCategories();

    }

    public function updateDescription(){
        return \App\Services\UpdateService::updateDescription();

    }
}
