<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\PropertyValue;

class ListController extends Controller
{
    public function properties()
    {
        $properties = Property::orderBy('name')->get(['id', 'name']);
        return response()->json([
          'properties' => $properties
        ]);
    }

    public function selectedPropertyValues(Property $property)
    {
        $values = $property->values()->orderBy('name')->get(['id', 'name', 'property_id']);
        return response()->json([
          'values' => $values
        ]);
    }
}
