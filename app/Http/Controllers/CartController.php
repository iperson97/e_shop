<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\{DeliveryMethod, PaymentMethod, City, Product, User, Order};
use App\Http\Requests\CheckoutRequest;
use Darryldecode\Cart\CartCondition;
use Illuminate\Support\Facades\Mail;
use App\Mail\{OrderCheckout, SendUserPassword, Manager};
use App\Jobs\Checkout;
use Cart;
use Auth;

class CartController extends Controller
{
    public function index()
    {
        $items = [];

        Cart::getContent()->each(function($item) use (&$items) {
            $items[] = $item;
        });

        return Inertia::render('Eshop/Cart/Items', compact('items'));
    }

    public function count()
    {
        $cartCount = Cart::getContent()->count();

        return response()->json([
            'count' => $cartCount,
        ]);
    }

    public function add(Request $request)
    {
        $product = Product::find($request->id);
        $quantity = $request->quantity ? $request->quantity : 1;
        $item = Cart::add([
            'id' => $product->id,
            'name' => $product->name,
            'price' => $product->price,
            'quantity' => $quantity,
            'attributes' => [],
            'associatedModel' => $product
        ]);

        $product->quantity = $quantity;

        return response()->json([
            'product' => $product,
        ], 201);
    }

    public function update(Request $request, Product $product)
    {
        Cart::update($product->id, [
        	'quantity' => [
                'relative' => false,
                'value' => $request->quantity
            ]
        ]);

        return response()->json(null, 201);
    }

    public function delete($id)
    {

        Cart::remove($id);

        return response()->json(null, 204);
    }

    public function details()
    {
        return response()->json([
            'details' => [
                'total_quantity' => Cart::getTotalQuantity(),
                'sub_total' => Cart::getSubTotal(),
                'total' => Cart::getTotal(),
            ],
        ], 201);
    }

    public function checkoutView()
    {
        if (Cart::isEmpty()) {
            return redirect()->back();
        }

        Cart::getContent()->each(function($item) use (&$items) {
            $items[] = $item;
        });
        $deliveryMethods = DeliveryMethod::get(['id', 'name']);
        $paymentMethods = PaymentMethod::get(['id', 'name']);
        $cities = City::get(['id', 'name']);

        return Inertia::render('Eshop/Cart/Checkout', compact('items', 'deliveryMethods', 'paymentMethods', 'cities'));
    }

    public function checkout(CheckoutRequest $request)
    {
        if (Cart::isEmpty()) {
            abort(403);
        }

        $user = Auth::user();
        if ($user) {
            $user->update($request->validated());
        } else {
            $user = User::where('email', $request->email)->first();
            if ($user) {
                $user->update($request->validated());
            } else {
                $user = User::create($request->validated());
            }

            Mail::to($request->email)->queue(new SendUserPassword($request->password));
        }

        $order = $user->orders()->create([
            'delivery_method_id' => $request->delivery_method_id,
            'payment_method_id' => $request->payment_method_id,
            'notes' => $request->notes,
            'sub_total' => Cart::getTotal(),
            'total' => Cart::getTotal(),
        ]);

        if ($order) {
            $order->update([
                'total' => (int) $order->total + (int) $order->deliveryMethod->price
            ]);
            $items = [];
            Cart::getContent()->each(function($item) use (&$items) {
                $items[] = [
                    'product_id' => $item->id,
                    'quantity' => $item->quantity,
                    'price' => $item->associatedModel->price,
                    'total' => $item->getPriceSum(),
                ];
            });
            $order->products()->createMany($items);

            if ($request->validated()['address']) {
                $order->address()->create($request->validated()['address']);
            }
            Checkout::dispatch($user->email, $order);
            Cart::clear();
        }

        return response()->json(null, 201);
    }
}
