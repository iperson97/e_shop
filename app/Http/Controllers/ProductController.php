<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreReviewRequest;
use Inertia\Inertia;
use App\Models\{Category, Product, Order, Brand, Property, PropertyValue};
use Illuminate\Support\Facades\Http;
use App\Jobs\CreateProduct;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    /*
     v1/ показывает только товары первых детей
*/
    public function index2(Request $request, Category $category)
    {
        $category->load('children');
        if ($category->children) {
            $categoryIds = $category->children->pluck('id');
        }
        $categoryIds[] = $category->id;

        $products = Product::whereIn('category_id', $categoryIds)->with('brand')->onlyAvailable()->filter($request)->paginate(9);

        return response()->json([
            'products' => $products,
        ], 200);
    }
    public function index(Request $request, Category $category)
    {
        $category->load('children');
        if ($category->children) {
            $categoryIds = $category->children->pluck('id');
        }
        $categoryIds[] = $category->id;

        $products = Product::whereIn('category_id', $categoryIds)->with('brand')->onlyAvailable()->filter($request)->count();
        if($products>0){
            $products = Product::whereIn('category_id', $categoryIds)->with('brand')->onlyAvailable()->filter($request)->paginate(9);
            return response()->json([
                'products' => $products,
            ], 200);
        }
        $category->load('children');
        if ($category->children) {
            $categoryIds = $category->children->pluck('id');
            foreach ($category->children as $child){
                $child->load('children');
                foreach ($category->children->pluck('id') as $id){
                    $categoryIds[]=$id;
                }
                if ($child->children) {
                    $categoryIds = $child->children->pluck('id');
                    foreach ($child->children as $child2){
                        $child2->load('children');
                        foreach ($child2->children->pluck('id') as $id){
                            $categoryIds[]=$id;
                        }
                    }
                }
            }
        }
        $categoryIds[] = $category->id;
        $products = Product::whereIn('category_id', $categoryIds)->with('brand')->onlyAvailable()->filter($request)->paginate(9);
        return response()->json([
            'products' => $products,
        ], 200);
    }

    public function view(Request $request, Category $category)
    {
        $category->load('children', 'parent');
        $categoryIds = $category->parent_id ? [$category->id] : $category->children->pluck('id');
        return Inertia::render('Eshop/Products/Products', compact('category'));
    }

    public function search(Request $request)
    {
        $query = $request->q;
        return Inertia::render('Eshop/Products/SearchProducts', compact('query'));
    }

    public function searchProducts(Request $request)
    {
        $products = [];
        $count = 0;
        if ($request->q) {
            $productQuery = Product::filter($request)->onlyAvailable();
            $products = $productQuery->paginate(9);
            $count = $productQuery->count();
        }
        return response()->json([
            'products' => $products,
            'count' => $count,
        ], 200);
    }

    public function catalog(Request $request)
    {
        return Inertia::render('Eshop/Products/Catalog');
    }

    public function recommendations(Request $request)
    {
        $products = Product::recommendations()->limit($request->limit ?? 8)->get();

        return response()->json([
            'products' => $products,
        ], 200);
    }

    public function newItems(Request $request)
    {
        $products = Product::newItems()->limit($request->limit ?? 4)->get();

        return response()->json([
            'products' => $products,
        ], 200);
    }

    public function show(Product $product)
    {
        $product->load('category', 'reviews', 'reviews.usedTime','reviews.user', 'propertyValues.property.group', 'brand');
        $product->quantity = 1;

        return Inertia::render('Eshop/Products/Product', compact('product'));
    }

    public function fastOrder(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'max:255'],
            'phone' => ['required', 'numeric', 'digits:11'],
            'notes' => ['nullable'],
        ]);

        $validatedData['total'] = (int) $request->product['quantity'] * (int) $request->product['price'];
        $order = Order::create($validatedData);
        if ($order && $request->product) {
            $order->products()->create([
                'product_id' => $request->product['id'],
                'quantity' => $request->product['quantity'],
                'price' => $request->product['price'],
                'total' => (int) $request->product['quantity'] * (int) $request->product['price'],
            ]);
        }

        return response()->json(null, 201);
    }

    public function review(Product $product)
    {
        $product->load('category');
        $usedTimes = \App\Models\ProductUsedTime::get();
        return Inertia::render('Eshop/Products/Review', compact('product', 'usedTimes'));
    }

    public function storeReview(StoreReviewRequest $request, Product $product)
    {
        $product->reviews()->create($request->validated());
        return Inertia::location(route('products.show', $product->slug));
    }

    // public function test(Request $request)
    // {
    //     $product = Product::where('brand_id', 45)->first();
    //     $categories = $product->brand->categories->pluck('id')->all();
    //     if (is_numeric($product->brand->percentage_markup)) {
    //         if (in_array($product->category_id, $categories)) {
    //             $brandCategory = $product->brand->categories->where('id', $product->category_id)->first();
    //             $percentage = $brandCategory->pivot->percentage_markup ?? 10;
    //         } else {
    //             $percentage = $product->brand->percentage_markup;
    //         }
    //     }
    //
    //     echo $percentage;
    //
    // }
}
