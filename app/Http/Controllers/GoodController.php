<?php

namespace App\Http\Controllers;

use App\Models\annul;
use App\Models\DataHistory;
use App\Models\Good;

class GoodController extends Controller
{
    public function goodsCreate(){
        $response = [];
        $i=0;
        while(true){
            $offset = 250*$i;
            $url = "https://api.al-style.kz/api/elements?access-token=G1JPcjRiSLTbKUeaYw5K0MoBS-CxsbaV&limit=250&offset=".$offset;
            $result = Http::timeout(10)->get($url);
            if($result == '[]'){
                break;
            }
            $result = ''.$result;
            $result = json_decode($result);
            foreach($result as $item){
                return $item;
                if($item->price1<1001){
                    $item->price1*=2.5;
                }else if($item->price1<3001){
                    $item->price1*=2;
                }else if($item->price1<5001){
                    $item->price1*=1.5;
                }else if($item->price1<8001){
                    $item->price1*=1.4;
                }else if($item->price1<15001){
                    $item->price1*=1.25;
                }else if($item->price1<30001){
                    $item->price1*=1.18;
                }else{
                    $item->price1*=1.15;
                }
                array_push($response,$item);
            }
            $i++;
            if($i>100){
                break;
            }
        }
        return $response;

    }
}
