<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterCode;
use App\Mail\SendUserPassword;
use App\Models\ConfirmCode;
use App\Models\User;
use Auth;

class AuthController extends Controller
{
    public function registerCode(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'max:255', 'unique:users'],
        ]);

        $code = ConfirmCode::create([
            'email' => $request->email,
            'code' => rand(1000, 9999)
        ]);

        Mail::to($request->email)->queue(new RegisterCode($code));

        return response(null, 201);
    }

    public function confirmCode(Request $request)
    {
        $request->validate([
            'code' => ['required', 'numeric', 'digits:4'],
        ]);

        $code = ConfirmCode::where('email', $request->email)->where('code', $request->code)->first();
        if ($code) {
            ConfirmCode::where('email', $request->email)->delete();
            $pass = Str::random(8);
            $user = User::create([
                'email' => $request->email,
                'name' => 'user',
                'password' => $pass,
            ]);

            Auth::login($user);

            Mail::to($request->email)->queue(new SendUserPassword($pass));

            return response(null, 201);
        } else {
          return response([
              'errors' => ['code' => ['код недействительный']],
          ], 422);
        }
    }
}
