<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth;

class UserController extends Controller
{
    public function index()
    {
        return redirect()->route('personal.profile');
    }

    public function profile()
    {
        return Inertia::render('Eshop/User/Profile');
    }

    public function updatePassword(Request $request)
    {
        return Inertia::render('Eshop/User/UpdatePassword');
    }

    public function orders(Request $request)
    {
        $orders = Auth::user()->orders()->with('products', 'products.product')->get();
        return Inertia::render('Eshop/User/Orders', compact('orders'));
    }
}
