<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Inertia\Inertia;
use Auth;

class ProductComparisonController extends Controller
{
    public function index(Request $request)
    {
        $products = [];
        $propertyValues = [];
        if (session()->exists('items_comparison')) {
            $products = Product::whereIn('id', session('items_comparison'))->with('brand')->get();
            $propArr = [];
            foreach ($products as $key => $value) {
                $propArr[] = $value->propertyValues->map(function($prop) use ($value) {
                    $prop->product_id = $value->id;
                    return $prop->only('id', 'name', 'product_id', 'property_id', 'property');
                });
            }

            $propertyValues = collect($propArr)->flatten(1);
        }
        return Inertia::render('Eshop/Comparison', compact('products', 'propertyValues'));
    }


    public function count(Request $request)
    {
        $count = 0;
        if ($request->session()->exists('items_comparison')) {
            $products = $request->session()->get('items_comparison');
            $count = count($products);
        }
        return response([
            'count' => $count
        ], 201);

    }

    public function add(Request $request, Product $product)
    {
        $products = $request->session()->get('items_comparison');
        if ($products) {
            if (count($products) < 4) {
                if (!in_array($product->id, $products)) {
                    $request->session()->push('items_comparison', $product->id);
                }
            }

        } else {
            $request->session()->put('items_comparison', []);
            $request->session()->push('items_comparison', $product->id);
        }
        return response(null, 201);
    }

    public function remove(Request $request, Product $product)
    {
        $products = $request->session()->get('items_comparison');
        if ($products) {
            $found_key = array_search($product->id, $products);
            unset($products[$found_key]);
            $request->session()->put('items_comparison', $products);
        }
    }
}
