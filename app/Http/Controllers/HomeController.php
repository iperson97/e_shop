<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Category;
use Illuminate\Support\Facades\Mail;
use App\Mail\Zayavka;

class HomeController extends Controller
{
    public function welcome()
    {
        return Inertia::render('Eshop/Welcome');
    }

    public function categoryList()
    {
        $categoriesQ = Category::select('id', 'slug', 'name', 'parent_id')->with('children')->whereNull('parent_id')->get();
        $categories['pc'] = $categoriesQ->where('id', 1)->first();
        $categories['komplektuyushhie'] = $categoriesQ->where('id', 2)->first();
        $categories['perifiriya'] = $categoriesQ->where('id', 3)->first();
        $categories['laptops'] = $categoriesQ->where('id', 4)->first();
        $categories['mebel'] = $categoriesQ->where('id', 5)->first();
        $categories['klimat'] = $categoriesQ->where('id', 1786)->first();
        $categories['tv'] = $categoriesQ->where('id', 44)->first();
        $categories['elektrotransport'] = $categoriesQ->where('id', 50)->first();

        return response()->json([
            'categories' => $categories,
        ], 200);
    }

    public function service()
    {
        return Inertia::render('Eshop/Service');
    }

    public function aboutUs()
    {
        return Inertia::render('Eshop/AboutUs');
    }

    public function aboutDelivery()
    {
        return Inertia::render('Eshop/AboutDelivery');
    }

    public function securityPolicy()
    {
        return Inertia::render('Eshop/SecurityPolicy');
    }

    public function termsAgreement()
    {
        return Inertia::render('Eshop/TermsAgreement');
    }

    public function contractOffer()
    {
        return Inertia::render('Eshop/ContractOffer');
    }

    public function zayavka(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'max:255'],
            'phone' => ['required', 'numeric', 'digits:11'],
            'question' => ['nullable'],
        ]);
        Mail::to(config('mail.mail_for_messeges'))->queue(new Zayavka($validatedData));
        return response(null, 201);
    }

}
