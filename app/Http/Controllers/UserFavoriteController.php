<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Inertia\Inertia;
use Auth;

class UserFavoriteController extends Controller
{
    public function index(Request $request)
    {
        $products = $request->session()->get('favorite_items');
        return Inertia::render('Eshop/Favorites', compact('products'));
    }


    public function count(Request $request)
    {
        $count = 0;
        if ($request->session()->exists('favorite_items')) {
            $products = $request->session()->get('favorite_items');
            $count = count($products);
        }
        return response([
            'count' => $count
        ], 201);

    }

    public function addOrRemove(Request $request, Product $product)
    {
        $products = $request->session()->get('favorite_items');
        $productArr = $product->toArray();
        if ($products) {
            $found_key = $this->searchForId($product->id, $products);
            if (is_null($found_key)) {
                $request->session()->push('favorite_items', $productArr);
            } else {
                unset($products[$found_key]);
                $request->session()->put('favorite_items', $products);
            }
        } else {
            $request->session()->put('favorite_items', []);
            $request->session()->push('favorite_items', $productArr);
        }
        return response(null, 201);
    }

    public function remove(Request $request, Product $product)
    {
        $products = $request->session()->get('favorite_items');
        if ($products) {
            $found_key = $this->searchForId($product->id, $products);
            unset($products[$found_key]);
            $request->session()->put('favorite_items', $products);
        }
    }

    public function searchForId($id, $array)
    {
        foreach ($array as $key => $val) {
            if ($val['id'] === $id) {
                return $key;
            }
        }
        return null;
    }
}
