<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreReviewRequest extends FormRequest
{
    protected $errorBag = 'storeReview';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'user_id' => Auth::id(),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating' => ['required'],
            'user_id' => ['required'],
            'used_time_id' => ['required'],
            'advantages' => ['required'],
            'disadvantages' => ['required'],
            'notes' => ['required'],
        ];
    }
}
