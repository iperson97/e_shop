<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;

class CheckoutRequest extends FormRequest
{

    protected $errorBag = 'orderCheckout';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $pass = Str::random(8);
        if (!Auth::user()) {
            $this->merge([
                'password' => $pass,
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        $addressNoValidate = [3,5];
        $validate = [
            'email' => ['required', 'email', 'max:255'],
            'password' => ['nullable', 'max:255'],
            'surname' => ['nullable', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'digits:11'],
            'payment_method_id' => ['required', 'integer'],
            'delivery_method_id' => ['required', 'integer'],
            'notes' => ['nullable'],
            'address.city_id' => ['required', 'integer'],
        ];

        if (!in_array(request()->delivery_method_id, $addressNoValidate)) {
            $addressValidate = [
                'address.street' => ['required', 'string', 'max:255'],
                'address.house' => ['required', 'string', 'max:255'],
                'address.entrance' => ['required', 'string', 'max:255'],
                'address.floor' => ['required', 'string', 'max:255'],
                'address.apartment' => ['required', 'string', 'max:255'],
            ];

            $validate = array_merge($validate, $addressValidate);
        }

        return $validate;
    }
}
