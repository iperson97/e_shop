<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ProductParser;

class CreateProductParser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $parsedData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($parsedData)
    {
        $this->parsedData = $parsedData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $productParser = ProductParser::updateOrCreate([
            'alstyle_id' => $this->parsedData['alstyle_id'],
            'category_id' => $this->parsedData['category_id'],
        ], [
            'data' => $this->parsedData['data'],
        ]);
    }
}
