<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;
use App\Models\Brand;
use Illuminate\Support\Facades\DB;

class CreateProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $parsedData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($parsedData)
    {
        $this->parsedData = $parsedData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product = Product::where('article', $this->parsedData['article'])->first();
        $brandId = null;
        $brand = null;
        if (isset($this->parsedData['brand']) && $this->parsedData['brand']) {
            $brand = Brand::where('name', $this->parsedData['brand'])->first();
            if ($brand) {
                $brandId = $brand->id;
            } else {
                $brand = Brand::create([
                    'name' => $this->parsedData['brand']
                ]);
                $brandId = $brand->id;
            }

            if ($this->parsedData['category_id'] == 11) {
                if ($this->parsedData['brand'] == 'No name') {
                    if (preg_match("/\bamd\b/i", $this->parsedData['name'])) {
                        $brand = Brand::where('name', 'amd')->first();
                        $brandId = $brand->id;
                    }
                }
            }
        }

        $quantity = preg_replace('/[^0-9]/', '', $this->parsedData['quantity']);


        if ($brand && is_numeric($brand->percentage_markup)) {
            $categories = $brand->categories->pluck('id')->all() ?? [];
            if (in_array($this->parsedData['category_id'], $categories)) {
                $brandCategory = $brand->categories->where('id', $this->parsedData['category_id'])->first();
                $percentage = $brandCategory->pivot->percentage_markup ?? 10;
            }
            else {
                $percentage = $brand->percentage_markup;
            }
        } else {
            $percentage = 10;
        }
        if (is_numeric($percentage)) {
            if ($percentage > 0) {
                $price = (int) $this->parsedData['price1'] + (((int)$percentage / 100) * (int) $this->parsedData['price1']);
            } else if ($percentage == 0) {
                $price = $this->parsedData['price1'];
            }
        }


        $prodArr = [
            'name' => $this->parsedData['name'],
            'brand_id' => $brandId,
            'category_id' => $this->parsedData['category_id'],
            'full_name' => $this->parsedData['full_name'],
            'article' => $this->parsedData['article'],
            'article_pn' => $this->parsedData['article_pn'],
            'images' => $this->parsedData['images'],
            'price' => $price,
            'status_id' => 2,
            'price1' => $this->parsedData['price1'],
            'left_quantity' => $quantity ?? 0,
            'isnew' => $this->parsedData['isnew'],
            'ishit' => $this->parsedData['ishit'],
            'ispromo' => $this->parsedData['ispromo'],
            'from' => 'al-style.kz',
            'url' => isset($this->parsedData['url']) ? $this->parsedData['url']: null
        ];
        if (isset($this->parsedData['description']) && $this->parsedData['description']) {
            $prodArr['description'] = $this->parsedData['description'];
        }
        if ($product) {
            $findedProd = Product::where('id', $product->id)->first();
            if (is_null($findedProd->status_id)) {
              $prodArr['status_id'] = 2;
            }
            $findedProd->update($prodArr);
        } else {
            Product::create($prodArr);
        }
    }
}
