<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;
use App\Models\Property;
use App\Models\PropertyValue;

class UpdateProductProperties implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $parsedData;
    protected $product;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product, $parsedData)
    {
        $this->product = $product;
        $this->parsedData = $parsedData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $propertyValues = [];
        if (isset($this->parsedData[0])) {
          $parsedData = $this->parsedData[0];
          if (is_array($parsedData) && count($parsedData) > 0) {
              foreach ($parsedData['properties'] as $key => $value) {
                  $propArr = ['Базовая единица', 'Новинка', 'Анонс', 'Код', 'Хит', 'Штрихкод', 'Порядок сортировки на сайте', 'Бренд', 'Полное наименование', 'Артикул-PartNumber', 'Дата последнего прихода', 'В упаковке', 'Тип упаковки'];
                  if (!in_array($key, $propArr)) {
                      $property = Property::firstOrCreate(['name' => $key]);
                      if ($property) {
                          $propertyValue = PropertyValue::firstOrCreate([
                              'name' => $value,
                              'property_id' => $property->id
                          ]);

                          if ($propertyValue) {
                              $propertyValues[] = $propertyValue->id;
                          }
                      }
                  }
              }

              if ($propertyValues) {
                  $this->product->propertyValues()->sync($propertyValues);
              }

              if (is_null($this->product->description) && $parsedData['detailtext']) {
                  $this->product->update([
                      'description' => $parsedData['detailtext'],
                  ]);
              }
          }
        }
    }
}
