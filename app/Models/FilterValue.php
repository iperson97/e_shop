<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FilterValue extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'name',
        'sort_order',
    ];

    public function filter()
    {
        return $this->belongsTo(Filter::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'filter_value_products', 'filter_value_id', 'product_id', 'id');
    }
}
