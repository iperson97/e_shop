<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'parent_id',
        'sort_order',
        'image',
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    public function childrenProducts()
    {
        return $this->hasManyThrough(
            'App\Models\Product', 'App\Models\Category',
            'parent_id', 'category_id', 'id'
        );
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class, 'category_property', 'category_id', 'property_id', 'id');
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'category_filters', 'category_id', 'filter_id', 'id');
    }
}
