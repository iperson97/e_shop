<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryMethod extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sort_order',
        'price',
    ];

    public function paymentMethods()
    {
        return $this->belongsToMany(PaymentMethod::class, 'delivery_payment', 'delivery_method_id', 'payment_method_id', 'id');
    }
}
