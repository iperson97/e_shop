<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Bitrix24;


class Order extends Model
{
    use HasFactory,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'status_id',
        'payment_method_id',
        'delivery_method_id',
        'total',
        'sub_total',
        'name',
        'phone',
        'notes',
        'paid',
        'delivered',
    ];

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function deliveryMethod()
    {
        return $this->belongsTo(DeliveryMethod::class);
    }

    public function address()
    {
        return $this->hasOne(OrderAddress::class);
    }

    public function addDealBitrix()
    {
        $resultDeal = Bitrix24::call('crm.deal.add', [
            'fields' => [
                "TITLE" => "Заказ от сайта https://eshop-online.kz/admin/orders/$this->id",
                "TYPE_ID" => "GOODS",
                "STAGE_ID" => "NEW",
                "OPENED" => "Y",
                "IS_NEW" => "Y",
                "CURRENCY_ID" => "KZT",
                "OPPORTUNITY" => $this->total,
            ]
       ]);
    }
}
