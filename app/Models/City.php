<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sort_order',
    ];

    public function deliveryMethods()
    {
        return $this->belongsToMany(DeliveryMethod::class, 'city_delivery', 'city_id', 'delivery_method_id', 'id');
    }
}
