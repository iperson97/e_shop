<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'property_group_id',
        'sort_order',
    ];

    public function values()
    {
        return $this->hasMany(PropertyValue::class);
    }

    public function group()
    {
        return $this->belongsTo(PropertyGroup::class, 'property_group_id');
    }
}
