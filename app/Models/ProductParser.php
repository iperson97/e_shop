<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductParser extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'alstyle_id',
        'data',
    ];

    protected $casts = [
        'data' => 'array',
    ];
}
