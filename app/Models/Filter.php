<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Filter extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'name',
        'sort_order',
        'type',
        'data',
    ];

    protected $casts = [
        'data' => 'array',
    ];

    public function values()
    {
        return $this->hasMany(FilterValue::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_filters', 'filter_id', 'category_id', 'id');
    }
}
