<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use App\Filters\ProductsFilter;
use App\Scopes\PriceScope;
use App\Scopes\QuantityScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Casts\ImageJsonCast;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'brand_id',
        'category_id',
        'full_name',
        'status_id',
        'slug',
        'article',
        'article_pn',
        'images',
        'description',
        'sort',
        'price',
        'old_price',
        'price1',
        'left_quantity',
        'isnew',
        'ishit',
        'ispromo',
        'isrecommend',
        'url',
        'from'
    ];

    protected $casts = [
        'images' => ImageJsonCast::class,
        'left_quantity' => 'integer'
    ];

    protected $appends = [
        'image',
        'is_available',
        'in_favorites',
        'rating',
        'available_status'
    ];

    protected $with = ['status'];

    public function getImageAttribute()
    {
        if (is_array($this->images)) {
            return count($this->images) > 0 ? $this->images[0]: '/images/mock.png';
        } else {
            $images = json_decode($this->images, true);
            return $images ? $images[0]: '/images/mock.png';
        }

    }

    public function getIsAvailableAttribute()
    {
        return $this->left_quantity !== 0 || $this->left_quantity !== '';
    }

    public function getAvailableStatusAttribute()
    {
        if ($this->is_available) {
          $status = 'В наличии';
          if (strpos($this->url, 'al-style') || strpos($this->image, 'al-style')) {
            $status = 'На складе';
          }
        } else {
          $status = 'Нет в наличии';
        }
        return $status;
    }

    public function getRatingAttribute()
    {
        return round($this->reviews()->avg('rating'), 2);
    }

    public function getInFavoritesAttribute()
    {
        if (session()->exists('favorite_items')) {
            return in_array($this->id, Arr::pluck(session('favorite_items'), 'id'));
        }
        return false;
    }

    public function scopeOnlyAvailable($query)
    {
        return $query->where('left_quantity', '>', 0);
    }

    public function scopeFilter($query, $request)
    {
        return (new ProductsFilter($request))->apply($query);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\ProductStatus', 'status_id')->withDefault();
    }

    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }

    public function propertyValues()
    {
        return $this->belongsToMany(PropertyValue::class, 'product_propertyvalue', 'product_id', 'property_value_id', 'id');
    }

    public function filterValues()
    {
        return $this->belongsToMany(FilterValue::class, 'filter_value_products', 'product_id', 'filter_value_id', 'id');
    }

    public function scopeNewItems($query)
    {
        return $query->where('isnew', 1);
    }

    public function scopeRecommendations($query)
    {
        return $query->where('isrecommend', 1);
    }
}
