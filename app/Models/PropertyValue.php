<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyValue extends Model
{
    use HasFactory,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'property_id',
        'sort_order',
    ];

    protected $with = ['property'];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_propertyvalue', 'property_value_id', 'product_id', 'id');
    }
}
