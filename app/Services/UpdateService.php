<?php


namespace App\Services;


use App\Models\Good;
use App\Models\Product;
use Illuminate\Support\Facades\Http;
use App\Models\Category;

class UpdateService
{
    public static function updateCategories(){

        $url = "https://al-style.kz/upload/catalog_export/al_style_catalog.php";
        $result = Http::timeout(600)->get($url);
        $array = explode('</category>', $result);
        $count = count($array);
        $categories = [];
        for($i=0;$i<$count-1;$i++){
            $pos = strripos($array[$i],'category id="')+13;
            $categories[$i]['id']= '';
            while($array[$i][$pos]!=='"'){
                $categories[$i]['id'].=$array[$i][$pos];
                $pos++;
            }
            $pos = strripos($array[$i],'parentId="');
            if($pos){
                $pos+=10;
                $categories[$i]['parentId']= '';
                while($array[$i][$pos]!=='"'){
                    $categories[$i]['parentId'].=$array[$i][$pos];
                    $pos++;
                }
                $categories[$i]['parentId'] = (int) $categories[$i]['parentId'];
            }else{
                $categories[$i]['parentId']= null;
            }
            $pos = strripos($array[$i],'">')+2;
            $categories[$i]['name']= '';
            while($array[$i][$pos]!=='<'){
                $categories[$i]['name'].=$array[$i][$pos];
                $pos++;
                if(!isset($array[$i][$pos])){
                    break;
                }
            }


        }
        $response = [];
        $i=0;
        foreach($categories as $category){
            $new_category = Category::where('slug','=','api-'.$category['id'])->first();
            if(!isset($new_category->id)){
                $new_category = new Category();
            }
            $new_category->name = $category['name'];
            $new_category->slug = 'api-'.$category['id'];
            if(isset($category['parentId'])){
                $parent = Category::where('slug','=','api-'.$category['parentId'])->first();
                if(isset($parent)){
                    $new_category->parent_id = $parent->id;

                }
            }
            $new_category->save();
            array_push($response,$new_category);
        }
        return $response;
    }
    public static function updateGoods(){
        $response = [];
        $i=0;
        while(true){
            $offset = 250*$i;
            $url = "https://api.al-style.kz/api/elements?access-token=G1JPcjRiSLTbKUeaYw5K0MoBS-CxsbaV&limit=250&offset=".$offset."&additional_fields=description,brand,images,url";
            $result = Http::timeout(10)->get($url);
            if($result == '[]'){
                break;
            }
            $result = ''.$result;
            $result = json_decode($result);
            foreach($result as $item){
                $product = Product::where('article','=',$item->article)->first();
                if(!isset($product->id)){
                    $product = new Product();
                    $product->article= $item->article;
                    $product->status_id = 3;
                }
                $product->slug = 'api-'.$item->article;
                $category =Category::where('slug','=','api-'.$item->category)->first();
                if(isset($category->id) && $category->id!== 37){
                    $product->category_id = $category->id;
                }
                $product->name = $item->name;
                $product->full_name= $item->full_name;
                $product->sort= $item->sort;
                $product->price1 = $item->price1;
                if(gettype($item->quantity)=='string'){
                    $item->quantity = substr($item->quantity,1);
                }
                $product->left_quantity = (int)$item->quantity;
                if($item->price1<1001){
                    $item->price1*=2.5;
                }else if($item->price1<3001){
                    $item->price1*=2;
                }else if($item->price1<5001){
                    $item->price1*=1.5;
                }else if($item->price1<8001){
                    $item->price1*=1.4;
                }else if($item->price1<15001){
                    $item->price1*=1.25;
                }else{
                    $item->price1*=1.18;
                }
                if(!isset($category->id) || $category->id!==37){
                    $product->price= round($item->price1);
                }
                $product->left_quantity= $item->quantity;
                $product->isnew= $item->isnew;
                $product->ishit= $item->ishit;
                $product->ispromo= $item->ispromo;
                $product->article_pn= $item->article_pn;
                $product->images= $item->images;
                if ($item->description){
                    $product->description= $item->description;
                }
                $product->url= $item->url;
                $product->from = 'al-style.kz';
                $product->save();
                array_push($response,$item); }
            $i++;

            if($i>200){
                break;
            }
        }
        return $response;
    }
    public static function updateDescription(){

        $response = [];
        for($i=0;$i<5;$i++) {
            $products = Product::whereNull('description')->where('slug', 'like', 'api-%')->limit(10)->get();
            $articles = '';
            foreach ($products as $product) {
                $response[] = $product->article;
                $articles .= $product->article . ',';
            }
            $articles = substr_replace($articles, "", -1);
            $url = "https://api.al-style.kz/api/element-info?access-token=G1JPcjRiSLTbKUeaYw5K0MoBS-CxsbaV&article=" . $articles . "&additional_fields=detailtext,properties";
            $results = Http::timeout(10)->get($url);
            $results = $results->json();
            foreach ($results as $result) {
                $product = Product::where('article', '=', $result['article'])->first();
                if ($result['detailtext'] != "") {
                    $product->description = $result['detailtext'];
                } else {
                    $product->description = "Описания нет";
                }
                $product->save();
            }
            foreach ($response as $article) {
                $product = Product::where('article', '=', $article)->first();
                if($product->description==null){
                    $product->description = "Описания нет";
                }
                $product->save();
            }
        }
        return $response;
    }
}
