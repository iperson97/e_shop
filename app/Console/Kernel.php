<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')->hourly();
        //$schedule->command('parse:categoryproducts')->hourly()->between('5:00', '23:59');
        //$schedule->command('update:product_qty_zero')->hourlyAt(20)->between('7:00', '23:59');
        //$schedule->command('parse:products')->hourlyAt(30)->between('5:00', '23:59');
        //$schedule->command('parse:productsdetail')->hourlyAt(45)->between('8:00', '23:59');
        //$schedule->command('parse_and_fix:product_price')->dailyAt('04:00');
        $schedule->call(function (){
            \App\Services\UpdateService::updateGoods();
        })->everyFifteenMinutes();
        $schedule->call(function (){
            \App\Services\UpdateService::updateDescription();
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
