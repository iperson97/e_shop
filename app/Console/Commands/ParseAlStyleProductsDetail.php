<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Product;
use App\Models\Property;
use App\Models\PropertyValue;
use App\Jobs\UpdateProductProperties;

class ParseAlStyleProductsDetail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:productsdetail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parse Al-style products detail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = Product::whereNotNull('article')->with('propertyValues')->get();
        $this->info('Start ParseAlStyleProductsDetail');
        $sleep = 0;
        foreach ($products as $key => $product) {
            ++$sleep;
            if ($sleep == config('parser.sleep_count')) {
                $this->info(config('parser.sleep_time').' сек');
                sleep(config('parser.sleep_time'));
                $sleep = 0;
            }

            $response = Http::get('https://api.al-style.kz/api/element-info', [
                'access-token' => config('parser.al_style_key'),
                'article' => $product->article,
                'additional_fields' => 'description,brand,weight,warranty,images,properties,detailtext,url',
            ]);

            if (is_array($response->json())) {
                UpdateProductProperties::dispatch($product, $response->json());
            }
            $this->info($product->name);
        }
        $this->info('End ParseAlStyleProductsDetail');
        return 0;
    }
}
