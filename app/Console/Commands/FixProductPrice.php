<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Product;
use App\Jobs\UpdateProduct;

class FixProductPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse_and_fix:product_price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсим и добавляем наценку на цену товара';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = Product::whereNotNull('article')->get();
        $this->info('Start FixProductPrice');
        $sleep = 0;
        foreach ($products as $key => $product) {
            ++$sleep;
            if ($sleep == config('parser.sleep_count')) {
                $this->info(config('parser.sleep_time').' сек');
                sleep(config('parser.sleep_time'));
                $sleep = 0;
            }

            $response = Http::get('https://api.al-style.kz/api/element-info', [
                'access-token' => config('parser.al_style_key'),
                'article' => $product->article,
                'additional_fields' => '',
            ]);

            if (is_array($response->json()) && !empty($response->json())) {
                $data = $response->json()[0];


                if ($product->brand && is_numeric($product->brand->percentage_markup)) {
                    $categories = $product->brand->categories->pluck('id')->all() ?? [];
                    if (in_array($product->category_id, $categories)) {
                        $brandCategory = $product->brand->categories->where('id', $product->category_id)->first();
                        $percentage = $brandCategory->pivot->percentage_markup ?? 10;
                    } else {
                        $percentage = $product->brand->percentage_markup;
                    }
                } else {
                    $percentage = 10;
                }
                if (is_numeric($percentage)) {
                    if ($percentage > 0) {
                        $price = (int) $data['price1'] + (((int)$percentage / 100) * (int) $data['price1']);
                    } else if ($percentage == 0) {
                        $price = $this->$data['price1'];
                    }
                    if ($product->price != $price) {
                        UpdateProduct::dispatch($product, ['price' => $price]);
                    }
                }

            }
        }

        $this->info('End FixProductPrice');
        return 0;
    }
}
