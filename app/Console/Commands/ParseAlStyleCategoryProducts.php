<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Category;
use Illuminate\Support\Facades\Http;
use App\Jobs\CreateProductParser;

class ParseAlStyleCategoryProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:categoryproducts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parse Al-style products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categories = Category::whereNotNull('alstyle_ids')->get();
        $this->info('Start ParseAlStyleCategoryProducts');
        $sleep = 0;
        foreach ($categories as $key => $category) {
            $alstyle_ids = json_decode($category->alstyle_ids);
            ++$sleep;
            if ($sleep == config('parser.sleep_count')) {
                $this->info(config('parser.sleep_time').' сек');
                sleep(config('parser.sleep_time'));
                $sleep = 0;
            }
            foreach ($alstyle_ids as $key => $categoryId) {
                $limit = 100;
                $data = $this->parser($categoryId, $limit);
//                $this->info(count($data));
                if ($data) {
                    CreateProductParser::dispatch([
                        'alstyle_id' => $categoryId,
                        'category_id' => $category->id,
                        'data' => $data,
                    ]);
                }
            }
        }
        $this->info('END ParseAlStyleCategoryProducts');
        return 0;
    }

    public function parser($categoryId, $limit, $offset = 0, $data = [])
    {

        $response = Http::get('https://api.al-style.kz/api/elements', [
            'access-token' => config('parser.al_style_key'),
            'category' => $categoryId,
            'limit' => $limit,
            'offset' => $offset,
            'additional_fields' => 'description,brand,weight,warranty,images,url',
        ]);
        if (count($response->json()) > 0) {
            $offset += $limit;
            $mergedData = array_merge($data, $response->json());
            return $this->parser($categoryId, $limit, $offset, $mergedData);
        } else {
            return array_merge($data, $response->json());
        }

    }
}
