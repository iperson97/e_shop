<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DeleteDublicateProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:dublicate_products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удалить дубликаты продуктов с одинокывым article';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = DB::table('products')->whereIn('article', function ( $query ) {
            $query->select('article')->from('products')->groupBy('article')->havingRaw('count(*) > 1');
        })->orderBy('article')->get();

        foreach ($products as $key => $product) {
            $this->info($product->name);
            DB::table('products')->where('id', $product->id)->where('left_quantity', '>', 0)->delete();
        }

        return 0;
    }
}
