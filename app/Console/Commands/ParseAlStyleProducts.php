<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\ProductParser;
use App\Jobs\CreateProduct;

class ParseAlStyleProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parse Al-style products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categories = ProductParser::whereNotNull('data')->get();
        $this->info('Start ParseAlStyleProducts');
        foreach ($categories as $key => $category) {
            foreach ($category->data as $key => $value) {
                $value['category_id'] = $category->category_id;
                CreateProduct::dispatch($value);
            }
        }
        $this->info('End ParseAlStyleProducts');
        return 0;
    }
}
