<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixProductQuantity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:products_quantity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Исправить количество товаров';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = DB::table('products')->where('left_quantity', 'like', '%>%')->orderBy('left_quantity')->get();
        foreach ($products as $key => $product) {
            $quantity = preg_replace('/[^0-9]/', '', $product->left_quantity);
            DB::table('products')->where('id', $product->id)->update([
                'left_quantity' => $quantity
            ]);
        }
        return 0;
    }
}
