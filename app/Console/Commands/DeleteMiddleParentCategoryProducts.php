<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Category;

class DeleteMiddleParentCategoryProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:category_products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удаляем продукты который находиться в промежуточном категорий';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categories = Category::whereNotNull('parent_id')->whereHas('children', function ($query) {
            $query->whereNotNull('parent_id');
        }, '>=', 1)->get();
        foreach ($categories as $key => $value) {
            $value->products()->delete();
        }
        return 0;
    }
}
