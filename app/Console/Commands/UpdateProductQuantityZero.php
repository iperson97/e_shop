<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ProductParser;
use App\Models\Product;
use App\Jobs\UpdateProductQuantity;
use Illuminate\Support\Facades\Http;

class UpdateProductQuantityZero extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:product_qty_zero';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' Обнавляем кол-во продукта на ноль';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = Product::whereNotNull('article')->get();
        $articles = [];
        $sleep = 0;
        foreach ($products as $key => $product) {
            ++$sleep;
            if ($sleep == config('parser.sleep_count')) {
                $this->info(config('parser.sleep_time') . ' сек');
                sleep(config('parser.sleep_time'));
                $sleep = 0;
            }

            $response = Http::get('https://api.al-style.kz/api/quantity', [
                'access-token' => config('parser.al_style_key'),
                'article' => $product->article
            ]);
            $articles[$product->article] = $product->left_quantity;
            foreach ($response->json() as $key => $value) {
                if ($key === 'status' && $value === 404) {
                    $articles[$product->article] = 0;
                }
                if ($key === (int)$product->article) {
                    $regexed_quantity = (int)preg_replace('/[^0-9]/', '', $value);
                    $articles[$key] = $regexed_quantity;
                }
            }
        }

        $articles_quantity_zero = array_filter($articles, function ($item) {
            return $item === 0;
        });

        UpdateProductQuantity::dispatch(array_keys($articles_quantity_zero));
        $this->info('End UpdateProductQuantityZero');

        return 0;
    }
}
