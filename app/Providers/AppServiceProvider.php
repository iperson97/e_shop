<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;
use App\Models\Category;
use App\Observers\ProductObserver;
use App\Models\Product;
use TCG\Voyager\Facades\Voyager;
use App\FormFields\MultipleImagesField;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('bitrix24', 'App\Services\Bitrix24');
        Product::observe(ProductObserver::class);
        Inertia::share('globalCategories', function () {
            return Category::select('slug', 'name', 'sort_order', 'parent_id')->whereNull('parent_id')->get();
        });

        Voyager::addFormField(MultipleImagesField::class);
    }
}
