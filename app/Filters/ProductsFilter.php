<?php

namespace App\Filters;

use App\Models\PropertyValue;
use App\Models\FilterValue;

class ProductsFilter extends QueryFilter
{
    public function q($value)
    {
        $this->builder->where('name', 'LIKE', "%$value%")
              ->orWhere('full_name', 'LIKE', "%$value%")
              ->orWhere('article', 'LIKE', "%$value%");
    }

    public function brand_id($value)
    {
        $this->builder->where('brand_id', $value);
    }

    public function category_id($value)
    {
        $this->builder->where('category_id', $value);
    }

    public function brands($value)
    {
        $this->builder->whereIn('brand_id', $value);
    }

    public function properties($propertyValues)
    {
        $properties = PropertyValue::whereIn('id', $propertyValues)->select('property_id')->groupBy('property_id')->get();

        foreach ($properties as $key => $value) {
            $this->builder->whereHas('propertyValues', function ($query) use ($propertyValues, $value) {
                $query->whereIn('id', $propertyValues)->where('property_id', $value->property->id);
            });
        }
    }

    public function options($filterValues)
    {
        $filters = FilterValue::whereIn('id', $filterValues)->select('filter_id')->groupBy('filter_id')->get();

        foreach ($filters as $key => $value) {
            $this->builder->whereHas('filterValues', function ($query) use ($filterValues, $value) {
                $query->whereIn('id', $filterValues)->where('filter_id', $value->filter->id);
            });
        }
    }

    public function optionsDiapason($filterValues)
    {
        foreach ($filterValues as $key => $value) {
            $value = json_decode($value);
            $this->builder->whereHas('filterValues', function ($query) use ($filterValues, $value) {
                $query->whereBetween('name', $value->ranges)->where('filter_id', $value->id);
            });
        }
    }

    public function start_price($value)
    {
        $this->builder->where('price', '>=', $value);
    }

    public function end_price($value)
    {
        $this->builder->where('price', '<=', $value);
    }

    public function orderby($value)
    {
        switch ($value) {
            case 'new':
                $this->builder->orderBy('created_at', 'desc');
                break;
            case 'price_cheaper':
                $this->builder->orderBy('price', 'asc');
                break;
            case 'price_expensive':
                $this->builder->orderBy('price', 'desc');
                break;
            case 'az':
                $this->builder->orderBy('name');
                break;
            default:
                $this->builder->orderBy('created_at');
                break;
        }

    }

}
