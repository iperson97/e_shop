<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class ImageJsonCast implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, $key, $value, $attributes)
    {
        return json_decode($value);
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, $key, $value, $attributes)
    {
        $pattern = '~[a-z]+://\S+~';
        if (is_array($value)) {
            foreach ($value as $key => $image) {
                if (preg_match_all($pattern, $image, $out) == 0) {
                    $value[$key] = config('app.url').'/storage/'.$image;
                }
            }
            $value = json_encode($value);
        } else if($images = json_decode($value)) {
            foreach ($images as $key => $image) {
                if (preg_match_all($pattern, $image, $out) == 0) {
                    $images[$key] = config('app.url').'/storage/'.$image;
                }
            }
            $value = json_encode($images);
        }
        return $value;
    }
}
